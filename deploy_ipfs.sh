
IPFS_deroswap_FOLDER_IPNS="k51qzi5uqu5dlmeekhaokr511s5giok8vxwudv2090cx4rgg4ab4okl3uzfquh"

echo 'Deploying...'
FOLDER_NAME='derodex'
ipfs files mkdir -p /$FOLDER_NAME
cat dist/index.html | ipfs files write --create /$FOLDER_NAME/index.html
echo 'Deploy source'
cat source.tar.gz | ipfs files write --create /$FOLDER_NAME/source.tar.gz

echo 'Publish new hash to IPNS name'
DEROSWAP_FOLDER=`ipfs files ls -l | grep "$FOLDER_NAME" | awk '{print $2}'`
NEW_IPNS=`ipfs name publish $DEROSWAP_FOLDER | awk '{print $3}'`
NEW_IPNS=${NEW_IPNS%:*}

echo
echo "* Mainnet:"
echo "/ipns/$NEW_IPNS"
echo "http://ipfs.io/ipns/$NEW_IPNS"
echo ""
echo "* Source code:"
echo "/ipns/$NEW_IPNS/source.tar.gz"
echo "http://ipfs.io/ipns/$NEW_IPNS/source.tar.gz"
echo ""
