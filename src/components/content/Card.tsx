/** Card for deroswap with a title and a settings button */

import { ImCog } from "react-icons/im";
import { IconButton } from "theme-ui";
import { colors } from "../../theme";
import { Spacer } from "./Spacer";

export function Card(props: any) {
  const defaultWidth = "320px";
  const width = props.width || defaultWidth;

  return (
    <div
      className="drop-shadow"
      style={{
        paddingLeft: `calc(50vw - ${width})`,
        paddingRight: `calc(50vw - ${width})`,
      }}
    >
      <section
        style={{
          width: "100%",
          marginLeft: "1em",
          marginRight: "1em",
          background: colors.blackAlpha(500),
          borderRadius: "8px",
          flexDirection: "column",
        }}
      >
        <div
          style={{
            justifyContent: "space-between",
            lineHeight: "2em",
            margin: ".5em 1em",
          }}
        >
          <div>{props.title?.toUpperCase()}</div>
          <Spacer />
          {/*<IconButton>
            <ImCog color={colors.whiteAlpha(700)} />
        </IconButton>*/}
        </div>
        <div
          style={{
            flexDirection: "column",
            flexGrow: '1',
            gap:'1em',
            margin: '1em'
          }}
        >
          {props.children}
        </div>
      </section>
    </div>
  );
}
