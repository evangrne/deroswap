import { createContext, Dispatch, useContext } from "react";

import reactLogo from "./assets/react.svg";
import "./App.css";
import * as _package from '../package.json';

// @ts-ignore
import DeroBridgeApi from "dero-rpc-bridge-api";

import { ToastContainer, Slide } from "react-toastify";
import "react-toastify/ReactToastify.min.css";

import { ConnectionStatus, ConnectionAction, AppState } from "./types";
import { Header } from "./components/header/Header";
import { Route, Routes } from "react-router-dom";
import { routes } from "./routes";
import Home from "./routes/Home";
import { DeroBridgeApiContextProvider } from "./utils/DeroBridgeApiContext";
import { PieSwapDataProvider } from "./utils/PieswapDataContext";
import { ThemeProvider } from "theme-ui";
import { colors, theme } from "./theme";

function App() {
  // Dynamic Styles
  const bgGradient = {
    //in: 'rgba(29,26,96,1)',
    in: "#3631AEff",
    out: "rgba(6,5,19,1)",
  };

  // ------------ Component
  return (
    <ThemeProvider theme={theme}>
      <DeroBridgeApiContextProvider>
        <div
          className="App"
          style={{
            background: `radial-gradient(circle at top, ${bgGradient.in} 0%, ${bgGradient.out} 100%)`,
            flexDirection: "column",
            rowGap: "2em",
            justifyContent: "flex-start",
          }}
        >

          <PieSwapDataProvider>
            <Header />
            <Routes>
              <Route path="/" element={<Home />} />
              {routes.map((route) => {
                return (
                  <Route
                    key={route.label}
                    path={route.to}
                    element={<route.element />}
                  />
                );
              })}
            </Routes>
          </PieSwapDataProvider>
          <div style={{ alignSelf: 'flex-start', flexGrow: 1 }}><div style={{ alignSelf: 'flex-end', margin: '1em', fontSize: '8pt' }}>version {_package.version}</div></div>
        </div>

        <ToastContainer
          position="bottom-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          transition={Slide}
          limit={1}
          theme='light'
        />

      </DeroBridgeApiContextProvider>
    </ThemeProvider>
  );
}

export default App;
