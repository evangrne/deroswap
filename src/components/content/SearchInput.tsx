import { ChangeEvent } from "react";
import { colors } from "../../theme";

export type SearchInputProps = {
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  label: string;
};

export function SearchInput({ onChange, label }: SearchInputProps) {
  return <div style={{ gap: "1em", alignItems: "center", width:'100%' }}>
    <label>{label}</label>
    <input
      style={{
        background: colors.whiteAlpha(200),
        fontFamily: "monospace",
        fontWeight: "bold",
        padding: ".8em 1.5em",
        border: "none",
        borderRadius: "3em",
        fontSize: "1em",
        flexGrow: '1'
      }}
      type={"text"}
      onChange={onChange}
    ></input>
  </div>;
}