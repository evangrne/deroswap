import { useContext, useEffect, useReducer, useState } from "react";
import { ConnectionStatus } from "../types";
import { DeroBridgeApiContext } from "../utils/DeroBridgeApiContext";
import { wait } from "../utils/misc";
import { PieSwapDataContext } from "../utils/PieswapDataContext";
import { Line } from 'react-chartjs-2';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { colors } from "../theme";

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);



type History = {
    [height: number]: {
        [pairName: string]: number
    }
}

interface HistoryReducer {
    action: 'update' | 'reset';
    newData?: {
        pairName: string;
        height: number;
        value: number;
    }
}
function historyReducer(data: History, { action, newData }: HistoryReducer) {
    switch (action) {
        case 'update':
            if (newData) {
                return { ...data, [newData.height]: { ...data[newData.height], [newData.pairName]: newData.value } }
            }

        case 'reset':
            return initialHistory;
        default:
            console.warn('no change in data', { data }, { action }, { newData });
            return data;
            break;
    }
}

const initialHistory: History = {};

export default function Charts() {
    const { state } = useContext(DeroBridgeApiContext);
    const { data } = useContext(PieSwapDataContext);
    const [history, updateHistory] = useReducer(historyReducer, initialHistory);
    const [loadingHistory, setLoadingHistory] = useState(true);
    const daemonReady = state.daemon == ConnectionStatus.Ready;
    const [resolution, setResolution] = useState(32);
    const [start, setStart] = useState(1);
    const [height, setHeight] = useState(0);
    const [end, setEnd] = useState(0);
    const [pairs, setPairs] = useState<string[]>([]);
    const [refreshed, setRefreshed] = useState(0);
    const refresh = () => { setRefreshed(Math.random()) }



    const getHeight = async () => {
        return (await state.bridge.daemon('get-height')).data.result.height;
    }

    const getPair = async (pairContract: string, topoheight: number) => {
        const params = {
            scid: pairContract,
            variables: true,
            code: false,
            topoheight,
        }
        const response = await state.bridge.daemon('get-sc', params);
        return response;
    }

    const getRegistry = async (topoheight: number) => {
        const params = {
            scid: data.metadata.keys['dex.swap.registry'],
            variables: true,
            code: false,
            topoheight,
        }
        const response = await state.bridge.daemon('get-sc', params);

        return response;
    }


    useEffect(() => {
        if (daemonReady) {
            const s = async () => {
                const _end = await getHeight()
                setHeight(_end);
                setEnd(_end);
            };
            s();
            
        }
        if (data.metadata) {
            setSelectedPair(data.metadata.network == 'mainnet' ? 'DERO:DUSDT' : 'DERO:DDAI')
        }

    }, [daemonReady, data.metadata])


    const [progress, setProgress] = useState(0);

    useEffect(() => {
        updateHistory({ action: 'reset' })
        if (daemonReady && data.metadata.keys) {
            setLoadingHistory(true);


            const fetch = async () => {

                setProgress(0)
                let _progress = 0
                let step = Math.max(Math.floor((end - start) / resolution), 1);
                for (let topoHeight = end; topoHeight >= start; topoHeight -= step) {
                    await wait(2, true);
                    
                    _progress += 1/resolution
                    setProgress(_progress);
                    
                    

                    const stringkeys = (await getRegistry(topoHeight)).data.result.stringkeys;
                    if (stringkeys === undefined) {
                        continue;
                    }
                    const pairContracts = Object.keys(stringkeys).filter(k => k.startsWith('p:')).map(k => ({ name: k.substring(2), contract: stringkeys[k] }));


                    pairContracts.forEach(async ({ name, contract }) => {
                        
                        await wait(1, true);
                        const pairStringkeys = (await getPair(contract, topoHeight)).data.result.stringkeys;
                        const [name1, name2] = name.split(":");
                        const val1 = pairStringkeys.val1;
                        const val2 = pairStringkeys.val2;
                        const digits1 = stringkeys[`t:${name1}:d`];
                        const digits2 = stringkeys[`t:${name2}:d`];
                        const ratio = (val2 / Math.pow(10, digits2)) / (val1 / Math.pow(10, digits1));
                        if (!Number.isNaN(ratio)) {
                            updateHistory({ action: 'update', newData: { pairName: name, height: topoHeight, value: ratio } })
                        }
                    })

                }
                console.log({history});
                setLoadingHistory(false);
            }
            fetch();

        }
    }, [daemonReady, data.metadata.keys, resolution, refreshed]);


    useEffect(() => {
        if (history) {
            const last = Math.max(...Object.keys(history).map(k => Number(k)));
            if (history[last]) {
                setPairs(Object.keys(history[last]));
            }
        }
    }, [history])

    const lineOptions = {
        responsive: false,
        plugins: {
            legend: {
                display: false,
                position: 'top' as const,
            },
            title: {
                display: false,
                text: 'Chart.js Line Chart',
            },
        },
    };

    const [selectedPair, setSelectedPair] = useState('');
    const filtered = [selectedPair];
    const labels = Object.keys(history).map(key => Number(key));


    const lineData = (pairs && !loadingHistory ? {
        labels,
        datasets: pairs.filter(key => filtered ? filtered.includes(key) : key).map((pair: string) => {
            return {
                label: pair,
                data: labels.map((h: number) => history[h][pair]),
                borderColor: colors.sky_blue_crayola,
            }
        }),
    } : { labels: [], datasets: [] })


    const [timeHistory, setTimeHistory] = useState('all-time');

    return <div style={{ flexDirection: "column", justifyContent: "center", alignItems: "center", width: "100%" }}>

        {loadingHistory ? 'loading... ' + Math.floor(progress*100) + ' %' : <>
            <div style={{ gap: '2em', alignItems: 'center' }}>
                <label>Pair</label>
                <select value={selectedPair} onChange={(event) => setSelectedPair(event.target.value)}>
                    {pairs.map(d => <option key={d}>{d}</option>)}
                </select>
                <label>Resolution</label>
                <select onChange={(event) => setResolution(Number(event.target.value))} value={resolution}>
                    <option>32</option>
                    <option>64</option>
                    <option>128</option>
                    <option>256</option>
                </select>

                <label>History</label>
                <select onChange={(event) => {
                    
                    console.warn({_height: height, value:event.target.value});
                    setEnd(height);
                    setTimeHistory(event.target.value);
                    switch (event.target.value) {
                        case "24h":
                            setStart(Math.max(1, height - 4800)); 
                            break;
                        case "1w":
                            setStart(Math.max(1, height - 33600)); 
                            break;
                        case "1m":
                            setStart(Math.max(1, height - 148800)); 
                            break;
                        case "3m":
                            setStart(Math.max(1, height - 446400)); 
                            break;
                        case "1y":
                            setStart(Math.max(1, height - 1785600)); 
                            break;
                        case "all-time":
                            setStart(1);
                            break;

                        default:
                            break;
                    }
                    refresh();
                    
                }} value={timeHistory}>
                    <option>24h</option>
                    <option>1w</option>
                    <option>1m</option>
                    <option>3m</option>
                    <option>1y</option>
                    <option>all-time</option>
                </select>

                <div style={{ flexDirection: 'column' }}>
                    <div style={{ gap: '2em', alignItems: 'center' }}>
                        <label>Start Height</label>
                        <input type='number' min='1' max={height - resolution} value={start} onChange={(event) => { setStart(Math.max(1, Math.min(end, Number(event.target.value)))) }}></input>
                    </div>
                    <div style={{ gap: '2em', alignItems: 'center' }}>
                        <label>End Height</label>
                        <input type='number' min='1' max={height} value={end} onChange={(event) => { setEnd(Math.max(1, Math.min(height, Number(event.target.value)))) }}></input>
                    </div>
                </div>/ {height}
                <button onClick={refresh} style={{backgroundColor: colors.blue_pigment, color: 'white'}}>update</button>
            </div>
            <div><Line options={lineOptions} data={lineData} width='960px' height='480px' /></div>
        </>
        }
    </div>;
}
