import React, { createContext, Dispatch, useReducer } from "react";
import { ConnectionAction, AppState, ConnectionStatus } from "../types";

// @ts-ignore
import DeroBridgeApi from "dero-rpc-bridge-api";

// Create context
const initialState = {
  wallet: ConnectionStatus.NotConnected,
  daemon: ConnectionStatus.NotConnected,
  bridge: new DeroBridgeApi(),
};

type ContextType = {
  state: AppState;
  update: Dispatch<ConnectionAction>;
};

export const DeroBridgeApiContext = createContext<ContextType>({
  state: initialState,
  update: () => {},
});

// Reducer for App state management
function reducer(state: AppState, action: ConnectionAction) {
  switch (action) {
    case ConnectionAction.InitialisedConnection:
      return {
        bridge: state.bridge,
        daemon: ConnectionStatus.Connected,
        wallet: ConnectionStatus.Connected,
      };
    case ConnectionAction.HasWallet:
      return { ...state, wallet: ConnectionStatus.Ready };
    case ConnectionAction.HasDaemon:
      return { ...state, daemon: ConnectionStatus.Ready };
    case ConnectionAction.LostDaemon:
      return { ...state, daemon: ConnectionStatus.Failed };
    case ConnectionAction.LostWallet:
      return { ...state, wallet: ConnectionStatus.Failed };
    default:
      console.error(action, "is not a known action");
      return state;
  }
}

// Component wrapper
export function DeroBridgeApiContextProvider(props: any) {
  const [state, update] = useReducer(reducer, initialState);

  return (
    <DeroBridgeApiContext.Provider value={{ state, update }}>
      {props.children}
    </DeroBridgeApiContext.Provider>
  );
}
