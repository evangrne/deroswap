import {
  useContext,
  useEffect,
  useReducer,
  useState,
} from "react";

import { ImArrowDown } from "react-icons/im";

import { Card } from "../components/content/Card";
import { ConnectionStatus, SwapAction, SwapActionType, SwapStateType } from "../types";
import { DeroBridgeApiContext } from "../utils/DeroBridgeApiContext";
import { PieSwapDataContext, UserDataContext } from "../utils/PieswapDataContext";

import { SwapButton } from "../components/content/SwapButton";
import { AmountSelector } from "../components/content/AmountSelector";
import { findPair } from "../utils/misc";

export function getDigitsAndRatio(pairData: any, reverse: boolean) {
  const digitsFrom = reverse
    ? pairData.registry.digits2
    : pairData.registry.digits1;
  const digitsTo = reverse
    ? pairData.registry.digits1
    : pairData.registry.digits2;
  const ratio = reverse
    ? 1 / pairData.pairContract.ratio1to2
    : pairData.pairContract.ratio1to2;
  return { digitsFrom, digitsTo, ratio }
}

export function swapReducer(
  swapState: SwapStateType,
  { action, token, amount, data }: SwapActionType
) {
  switch (action) {
    case SwapAction.ChangeAmountFrom:
      if (amount !== undefined && data !== undefined) {

        const { pairData, reverse } = findPair(swapState.tokenFrom, swapState.tokenTo, data)
        if (pairData !== undefined && pairData.pairContract !== undefined) {
          if (pairData.pairContract.hasLiquidity) {
            const { digitsFrom, digitsTo, ratio } = getDigitsAndRatio(pairData, reverse)
            
            const [valueFrom, valueTo] = !reverse ? [pairData.pairContract.value1, pairData.pairContract.value2] : [pairData.pairContract.value2, pairData.pairContract.value1];
            const amountToWithSlippage = Math.floor(((amount * Math.pow(10, digitsFrom)) * valueTo) / (valueFrom + amount* Math.pow(10, digitsFrom))) / Math.pow(10, digitsTo);
            
            const newState: SwapStateType = {
              ...swapState,
              amountFrom: Number(amount.toFixed(digitsFrom)),
              amountTo: amountToWithSlippage,
            };
            return newState;
          } else {
          }
        }
        const newState: SwapStateType = { ...swapState, amountFrom: amount };
        return newState;
      }

    case SwapAction.ChangeAmountTo:
      if (amount !== undefined && data !== undefined) {
        const { pairData, reverse } = findPair(swapState.tokenFrom, swapState.tokenTo, data)

        if (pairData !== undefined && pairData.pairContract !== undefined) {
          if (pairData.pairContract.hasLiquidity) {
            const { digitsFrom, digitsTo, ratio } = getDigitsAndRatio(pairData, reverse)
            const newState: SwapStateType = {
              ...swapState,
              amountFrom: Number((amount / ratio).toFixed(digitsFrom)),
              amountTo: Number(amount.toFixed(digitsTo)),
            };
            return newState;
          } else {
          }
        }
        const newState: SwapStateType = { ...swapState, amountTo: amount };
        return newState;
      }

    case SwapAction.ChangeTokenFrom:
      if (token !== undefined) {
        const newState: SwapStateType = { ...swapState, tokenFrom: token };
        return newState;
      }

    case SwapAction.ChangeTokenTo:
      if (token !== undefined) {
        const newState: SwapStateType = { ...swapState, tokenTo: token };
        return newState;
      }
    default:
      console.warn(action, "not implemented. or value is undefined");

      return swapState;
  }
}

export default function Swap() {
  // Context data
  const { state, update } = useContext(DeroBridgeApiContext);
  const ready =
    state.daemon == ConnectionStatus.Ready &&
    state.wallet == ConnectionStatus.Ready;

  // Context loading
  const { data } = useContext(PieSwapDataContext);
  const { user } = useContext(UserDataContext);

  // Local state data
  const initialSwapState: SwapStateType = {
    tokenFrom: "DERO",
    tokenTo: "DDAI",
    amountFrom: 0.0,
    amountTo: 0.0,
  };
  const [swapState, updateSwapState] = useReducer(
    swapReducer,
    initialSwapState
  );

  const [info, setInfo] = useState({ text: "", hasLiquidity: false });
  const [slippage, setSlippage] = useState<number | null>(null);


  useEffect(() => {
    console.log('Swap.tsx', data);

  }, [data])

  useEffect(() => {
    const { pairData, reverse } = findPair(swapState.tokenFrom, swapState.tokenTo, data)

    if (pairData !== undefined && pairData.pairContract !== undefined) {
      const {digitsFrom, digitsTo, ratio} = getDigitsAndRatio(pairData, reverse);
      
      const hasLiquidity = pairData.pairContract.hasLiquidity;
      
      const _amountFrom = swapState.amountFrom * Math.pow(10, digitsFrom);
      const computeSlippage = (amount:number, contractVal: number) => 100 - (1 / (1 + amount / contractVal) * 100);
      
      const value = reverse ? pairData.pairContract.value2 : pairData.pairContract.value1;
    
      setSlippage(computeSlippage(_amountFrom, value));

      setInfo({
        text: !hasLiquidity
          ? "Pair has no liquidity."
          : `1 ${swapState.tokenFrom} ≈ ${ratio} ${swapState.tokenTo}`,
        hasLiquidity: hasLiquidity,
      });
    } else {
      setInfo({ text: "", hasLiquidity: false });
    }
  }, [data, state, swapState]);

  const switchTokens = () => {
    const amountFrom = swapState.amountFrom;
    const amountTo = swapState.amountTo;
    const tokenFrom = swapState.tokenFrom;
    const tokenTo = swapState.tokenTo;

    updateSwapState({
      action: SwapAction.ChangeTokenFrom,
      token: tokenTo,
      data,
    });
    updateSwapState({
      action: SwapAction.ChangeTokenTo,
      token: tokenFrom,
      data,
    });
    updateSwapState({
      action: SwapAction.ChangeAmountFrom,
      amount: amountTo,
      data,
    });
    updateSwapState({
      action: SwapAction.ChangeAmountTo,
      amount: amountFrom,
      data,
    });
  };



  const unavailableAmount = (() => {
    const userTokenDataFrom = user.balances.find((token) => token.name == swapState.tokenFrom)
    if (userTokenDataFrom) {
      const userBalanceFrom = userTokenDataFrom.balance / Math.pow(10, userTokenDataFrom.digits);
      return userBalanceFrom < swapState.amountFrom
    } else { return true; }
  })();

  const slippageDisplay = Number.isFinite(slippage) && slippage != null && slippage > 0.001 ? Math.round((slippage || 0) * 1000) / 1000 : null;

  // Dynamic styles
  const [wobble, setWobble] = useState(0);

  // ------------- Component
  if (ready) {
    return (
      <Card title="swap">
        <AmountSelector
          state={swapState}
          update={updateSwapState}
          _key="from"

        />
        <div
          onClick={() => {
            setWobble(1);
            switchTokens();
          }}
          onAnimationEnd={() => setWobble(0)}
          //@ts-ignore
          wobble={wobble}

        >
          <ImArrowDown />
        </div>
        <AmountSelector state={swapState} update={updateSwapState} _key="to" noValidCheck />

        <div>{info.text}</div>
        <a title={slippage?.toString()}><div>{slippage != null && swapState.amountTo > 0 ?
          <>Slippage : {slippage < .001 ? '< 0,001' : slippageDisplay} %</>
          : <></>
        }</div></a>

        <SwapButton disabled={!info.hasLiquidity || swapState.amountTo == 0 || unavailableAmount} swapState={swapState} />
      </Card>
    );
  }
  return <>Wallet or daemon are not yet setup... </>;
}
