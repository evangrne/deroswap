import { ChangeEventHandler } from "react";
import { selectorInputStyles } from "./AmountSelector";

type PercentageInputProps = {
    onChange: ChangeEventHandler<HTMLInputElement>|undefined, 
    value: number|null
}

export function PercentageInput({onChange, value}:PercentageInputProps) {
  return (
    <input
      type={"number"}
      min={0}
      max={100}
      style={{ ...selectorInputStyles, maxWidth: "128px" }}
      placeholder="50"
      value={value == null ? "" : value}
      onChange={onChange}
    ></input>
  );
}
