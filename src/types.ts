import { ThemeUIStyleObject } from "theme-ui";
import { PieSwapData } from "./utils/PieswapDataContext";

export enum Entity {
  Wallet = "wallet",
  Daemon = "daemon",
}

// Connection reducer actions
export enum ConnectionAction {
  InitialisedConnection = "InitialisedConnection",
  HasWallet = "HasWallet",
  HasDaemon = "HasDaemon",
  LostWallet = "LostWallet",
  LostDaemon = "LostDaemon",
}

export enum DataAction {
  UpdateRegistry = "UpdateRegistry",
  UpdateBridgeRegistry = "UpdateBridgeRegistry",
  UpdatePairContract = "UpdatePairContract", 
  UpdateNetwork = "UpdateNetwork", 
  UpdateKeys = "UpdateKeys",
}

// Connection status of the daemon / the wallet
export enum ConnectionStatus {
  NotConnected = "NotConnected", // Bridge is not initialised
  Connected = "Connected", // Bridge is initialised but no communication has been established yet
  Ready = "Ready", // A call has beed made successfully
  Failed = "Failed", // The bridge init or a call has failed
}

// State
export type AppState = {
  [Entity.Wallet]: ConnectionStatus; // Wallet's connection status
  [Entity.Daemon]: ConnectionStatus; // Daemon's connection status
  bridge: any; // Wrapped bridge api object
};


export type SwapActionType = {
  action: SwapAction;
  data?: PieSwapData;
  token?: string;
  amount?: number;
};

export enum SwapAction {
  ChangeTokenFrom = "ChangeTokenFrom",
  ChangeTokenTo = "ChangeTokenTo",
  ChangeAmountFrom = "ChangeAmountFrom",
  ChangeAmountTo = "ChangeAmountTo",
  BeginSwap = "BeginSwap",
  ValidateSwap = "ValidateSwap",
  ConfirmSwap = "ConfirmSwap",
  CancelSwap = "CancelSwap",
}

export type SwapStateType = {
  tokenFrom: string;
  tokenTo: string;
  amountFrom: number;
  amountTo: number;
};

export type StyleDefinition = {
  [key: string]: ThemeUIStyleObject 
}