/** The amount selector is a combination of a token selector and controlled number input */


import {
  ChangeEvent,
  Dispatch,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import Modal from "react-modal";
import { colors, defaultModalStyles } from "../../theme";
import {
  StyleDefinition,
  SwapAction,
  SwapActionType,
  SwapStateType,
} from "../../types";
import { useModal } from "../../utils/misc";
import { PieSwapDataContext, UserDataContext } from "../../utils/PieswapDataContext";
import { SearchInput } from "./SearchInput";
import { TokenAvatar } from "./TokenAvatar";

type AmountSelectorProps = {
  state: SwapStateType;
  update: Dispatch<SwapActionType>;
  _key: "from" | "to";
  disableTokenSelection?: boolean;
  noValidCheck?: boolean;
};

// Base styles for the number input
export const selectorInputStyles = {
  height: "2.5em",
  background: colors.whiteAlpha(100),
  fontSize: "1.2em",
  fontFamily: "monospace",
  border: "none",
  outline: "none",
  borderRadius: ".5em",
  paddingLeft: "1em",
  width: "100%",
  color: colors.whiteAlpha(900),
};

// Component
export function AmountSelector({
  state,
  update,
  _key,
  disableTokenSelection,
  noValidCheck,
}: AmountSelectorProps) {
  // Extract the token and amount of this selector from the Swap State
  const [amount, token] = [
    _key == "from" ? state.amountFrom : state.amountTo,
    _key == "from" ? state.tokenFrom : state.tokenTo,
  ];

  // TODO conversion to fiat ?
  const fiat = "";

  // Context loading
  const { data } = useContext(PieSwapDataContext);
  const { user } = useContext(UserDataContext);

  // Dynamic Styles
  const selectorWidth = "7em";

  // Available amount
  const [availableTokenAmount, availableTokenAmountDisplay] = (
    () => {
      const tokenData = user.balances.find((a) => a.name == token)
      const realBalance = (tokenData?.balance || 0) / Math.pow(10, tokenData?.digits || 0);
      
      return [realBalance, realBalance.toFixed(tokenData?.digits)];
    }
  )();

  const getCurrent = () => _key == "from" ? SwapAction.ChangeAmountFrom : SwapAction.ChangeAmountTo;
  const updateCurrentAmount = (currentAmount: number) => {
    update({
      action: getCurrent(),// Depending on if it is from or to : we select the appropriate action for the swap state reducer
      amount: currentAmount,
      data: data,
    });
  }

  // When we change the value of the number input
  const onInputChange = (event: ChangeEvent<HTMLInputElement>) => {


    // Avoid letting the user put a number < 0
    // TODO check if it is useful
    const rawValue = event.target.value.startsWith("-")
      ? event.target.value.substring(1)
      : event.target.value;

    // If the user is typing a precision value we don't want to interfere
    if (!rawValue.endsWith(",") && !Number.isNaN(rawValue)) {
      // If value < than maximum available
      const available = Number(rawValue) <= availableTokenAmount;

      if (available && !noValidCheck) {
        updateCurrentAmount(Number(rawValue));
      }
    }
  };

  // Correct target token when changing the source if not available.
  useEffect(() => {
    if (_key == "to" && !availableTokens.includes(state.tokenTo)) {
      update({
        action: SwapAction.ChangeTokenTo,
        token: availableTokens.at(0),
      });
    }
  }, [state, data]);

  // returns a token avatar with its name
  const tokenWithAvatar = (name: string) => (
    <div style={{ alignItems: "center", gap: ".5em", color: 'white'}}>
      <TokenAvatar name={name} imageSize={36} />
      {name}
    </div>
  );

  // ------------- Modal

  // Modal state management
  const { isOpen, onOpen, onClose } = useModal();
  // Custom modal close
  const onModalClose = () => {
    onClose();
    // To reset the token search input text
    setModalSearchText("");
  };

  // On button click
  const onClick = useCallback(() => {
    // if token selection is enabled
    if (disableTokenSelection === undefined || !disableTokenSelection) {
      // open the modal
      onOpen();
    }
  }, [disableTokenSelection]);

  // Token search input text
  const [modalSearchText, setModalSearchText] = useState("");

  // Callback when modifying modal search text
  const onModalInputChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      setModalSearchText(event.target.value);
    },
    []
  );

  // List of available tokens.
  // If it's the selector on top we have access to every token that is in a pair contract
  // If it is the target token selector then we only give the possible pairs (ones that are registered)
  const availableTokens = (
    _key == "from"
      ? [...new Set(data.metadata.pairs.combinations.map((x) => x[0]))]
      : data.metadata.pairs.combinations
        .filter(([from, _]) => {
          return state.tokenFrom == from;
        })
        .map(([_, to]) => {
          return to;
        })
  ).filter((name) => {
    return modalSearchText == ""
      ? true
      : name.toLowerCase().includes(modalSearchText.toLowerCase());
  });

  // Callback When selecting a token
  const onTokenButtonClick = useCallback(
    (name: string) => {
      const action =
        _key == "from" ? SwapAction.ChangeTokenFrom : SwapAction.ChangeTokenTo;

      update({ action: action, token: name, data });
      update({
        action: SwapAction.ChangeAmountFrom,
        amount: 0,
        data,
      });
      update({
        action: SwapAction.ChangeAmountTo,
        amount: 0,
        data,
      });
      onModalClose();
    },
    [data]
  );

  const modalStyles: StyleDefinition = {
    layout: { flexDirection: "column", gap: "1em", fontFamily: "monospace" },
    searchInputContainer: { gap: "1em", alignItems: "center" },
    tokenListContainer: { flexWrap: "wrap", gap: "1em", width: "640px" },
    tokenButton: { background: colors.violet_blue_crayola },
    footer: { flexDir: "row", justifyContent: "flex-end" },
    cancelButton: { background: "transparent" },
  };

  const modal = (
    <Modal
      isOpen={isOpen}
      onAfterOpen={() => { }}
      onRequestClose={onModalClose}
      style={defaultModalStyles}
    >
      <div style={modalStyles.layout as any}>
        <div style={modalStyles.searchInputContainer as any}>
          <SearchInput
            onChange={onModalInputChange}
            label="Search for a token"
          />
        </div>
        <div style={modalStyles.tokenListContainer as any}>
          {/** List of available tokens */}
          {availableTokens.map((name) => (
            <div key={name}>
              <button
                style={modalStyles.tokenButton as any}
                onClick={() => onTokenButtonClick(name)}
              >
                {tokenWithAvatar(name)}
              </button>
            </div>
          ))}
        </div>
        <div style={modalStyles.footer as any}>
          <button style={modalStyles.cancelButton as any} onClick={onModalClose}>
            Cancel
          </button>
        </div>
      </div>
    </Modal>
  );


  // ----------------- Component
  const styles: StyleDefinition = {
    container: { flexGrow: "", alignItems: "center", width: '100%' },
    button: {
      height: "3em",
      width: selectorWidth,
      borderRadius: ".5em",
      background: colors.violet_blue_crayola,
    },
    fiatValueContainer: {
      width: selectorWidth,
      lineHeight: "2em",
      alignItems: "center",
      fontSize: ".8em",
      pl: "1em",
      justifyContent: "flex-start",
    },
  };

  const invalid = (amount > availableTokenAmount || availableTokenAmount == 0) && !noValidCheck;

  return (<div style={{ flexDirection: 'column', alignItems: 'flex-start', flexWrap: 'wrap', justifyContent: 'center' }}>

    <div style={styles.container as any}>
      <button style={styles.button as any} onClick={onClick}>
        {tokenWithAvatar(token)}
      </button>
      <input
        type={"number"}
        min={0}

        value={amount}
        onChange={onInputChange}
        style={{ ...selectorInputStyles, background: invalid ? colors.purple_pizzazz : selectorInputStyles.background }}
      />
      <div style={styles.fiatValueContainer as any}>{fiat ? ` ≈ ${fiat}` : ""}</div>
      {modal}
    </div>
    <div style={{ fontSize: '8pt', alignItems: "center", gap: '0.5em' }}>
      {!noValidCheck ? <>
        <div style={{ color: invalid ? colors.purple_pizzazz : 'inherit', fontWeight: 'bold' }}>max: {availableTokenAmountDisplay}</div>
        <div>
          <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900)}} onClick={() => { updateCurrentAmount(availableTokenAmount * .25) }}>25%</button>
          <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900) }} onClick={() => { updateCurrentAmount(availableTokenAmount * .5) }}>50%</button>
          <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900) }} onClick={() => { updateCurrentAmount(availableTokenAmount * .75) }}>75%</button>
          <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900) }} onClick={() => { updateCurrentAmount(availableTokenAmount) }}>max</button>
        </div>
      </> : <></>
      }
    </div>
  </div>
  );
}
