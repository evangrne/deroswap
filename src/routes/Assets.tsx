/** @jsxImportSource theme-ui */

import { useContext, useEffect, useState } from "react";
import { Card } from "../components/content/Card";
import { TokenAvatar } from "../components/content/TokenAvatar";
import { colors, defaultModalStyles } from "../theme";
import {
  PieSwapDataContext,
  UserBalanceListType,
  UserDataContext,
} from "../utils/PieswapDataContext";
import Modal from "react-modal";
import { useModal } from "../utils/misc";
import { StyleDefinition } from "../types";
import { selectorInputStyles } from "../components/content/AmountSelector";
import { scAssetTransfer } from "../api";
import { DeroBridgeApiContext } from "../utils/DeroBridgeApiContext";
import { toast } from "react-toastify";
import { SearchInput } from "../components/content/SearchInput";

export default function Assets() {
  const { state } = useContext(DeroBridgeApiContext);
  const { data } = useContext(PieSwapDataContext);
  const { user } = useContext(UserDataContext);

  const [assets, setAssets] = useState<UserBalanceListType>([]);
  useEffect(() => {
    setAssets([...user.balances]);
  }, [user.balances]);


  const [selectedAsset, setSelectedAsset] = useState<{
    name: string;
    asset: string;
    balance: number;
    digits: number;
  } | null>(null);

  // Modal state management
  const { isOpen, onOpen, onClose } = useModal();
  const onModalClose = () => {
    setAssetAmount(0);
    onClose();
  }

  const [assetAmount, setAssetAmount] = useState(0);
  const [deroWallet, setDeroWallet] = useState('');



  const modalStyles: StyleDefinition = {
    layout: { flexDirection: "column", gap: "1em", fontFamily: "monospace" },
    searchInputContainer: { gap: "1em", alignItems: "center" },
    tokenListContainer: { flexWrap: "wrap", gap: "1em", width: "640px" },
    tokenButton: { background: colors.violet_blue_crayola },
    footer: { flexDir: "row", justifyContent: "flex-end" },
    cancelButton: { background: "transparent" },
  };

  const selectedAssetBalanceDisplay = selectedAsset ? (selectedAsset.balance / Math.pow(10, selectedAsset.digits)).toFixed(selectedAsset.digits) : null;

  const isDeroWalletValid = deroWallet.length == 66 && ((deroWallet.startsWith('dero') && data.metadata.network == 'mainnet') || (deroWallet.startsWith('deto') && data.metadata.network == 'testnet'));

  const transferAsset = () => {
    if (selectedAsset) {
      const scid = selectedAsset.asset;
      const amount = Math.round(assetAmount * Math.pow(10, selectedAsset.digits));

      if (selectedAsset && assetAmount > 0 && isDeroWalletValid) {
        scAssetTransfer(
          scid,
          amount,
          deroWallet,
          state.bridge,
        ).then((result) => {
          console.log(result);
          onModalClose();
          toast.success('Successfully transferred.');

        }).catch(err => {
          console.error(err);
          toast.error('Something went wrong while transferring: ' + err);
        })
      }
    }
  }

  const modal = <Modal
    isOpen={isOpen}
    onAfterOpen={() => { }}
    onRequestClose={onModalClose}
    style={defaultModalStyles}
  >
    <div sx={modalStyles.layout}>
      <div style={{ gap: '1em', alignItems: 'center' }}> {selectedAsset ? <TokenAvatar imageSize={48} name={selectedAsset.name} /> : <></>}<h3>Transfer {selectedAsset?.name}</h3></div>
      <div style={{ gap: '1em', alignItems: 'center' }}>
        <label style={{ textAlign: "left", minWidth: '8rem' }}><b>Amount: </b></label>
        <div style={{ width: '100%', justifyContent: 'flex-start' }}>
          <input type="number" min='0' max={(selectedAssetBalanceDisplay == null ? '0' : selectedAssetBalanceDisplay)}
            value={assetAmount}
            onChange={(event) => { if (selectedAsset) setAssetAmount(Number(Number(event.target.value).toFixed(selectedAsset.digits))) }}
            style={{
              ...selectorInputStyles, paddingLeft: '1em', width: '10em',
              background: (Number(selectedAssetBalanceDisplay) >= assetAmount && !(selectedAsset?.balance == 0) ? selectorInputStyles.background : colors.purple_pizzazz)
            }} />
          <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900) }} onClick={() => { setAssetAmount(Number(selectedAssetBalanceDisplay) * .25) }}>25%</button>
          <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900)  }} onClick={() => { setAssetAmount(Number(selectedAssetBalanceDisplay) * .5) }}>50%</button>
          <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900)  }} onClick={() => { setAssetAmount(Number(selectedAssetBalanceDisplay) * .75) }}>75%</button>
          <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900)  }} onClick={() => { setAssetAmount(Number(selectedAssetBalanceDisplay)) }}>max</button>
        </div>
      </div>

      <div style={{ gap: '1.5em', width: '100%', justifyContent: 'flex-start', alignItems: 'center' }}>
        <label style={{ minWidth: '12rem', textAlign: 'left' }}><b>DERO wallet address</b></label>
        <input style={{ ...selectorInputStyles, paddingLeft: '1em', fontSize: '10pt', border: isDeroWalletValid ? 'inherit' : '2px solid ' + colors.purple_pizzazz }} onChange={(event) => { setDeroWallet(event.target.value) }} value={deroWallet} />
      </div>

      <div style={{ width: '100%', marginTop: '1em' }}>
        <button style={{background: colors.sky_blue_crayola, color: colors.blackAlpha(800) }} disabled={assetAmount == 0 || !isDeroWalletValid || Number(selectedAssetBalanceDisplay) < assetAmount} onClick={transferAsset}>Transfer Assets</button>
      </div>
    </div>
  </Modal>


  const [searchFilter, setSearchFilter] = useState<string | null>(null);
  const [owned, setOwned] = useState(true);

  return (
    <Card title="assets" width='240px'>
      {modal}
      <div sx={modalStyles.searchInputContainer}>
        <SearchInput
          onChange={(event) => { if (event.target.value == '') { setSearchFilter(null) } else { setSearchFilter(event.target.value) } }}
          label="Search"
        />
      </div>
      <div style={{ width: '100%', gap: '1em', justifyContent: 'flex-start' }}><input type='checkbox' checked={owned} onChange={(event) => setOwned(event.target.checked)} />owned</div>
      <table>
        <thead>
          <tr>
            <th>Asset</th>
            <th>Amount</th>
          </tr>
        </thead>
        <tbody>
          {(searchFilter ?
            assets.filter(asset => asset.name.toLowerCase().includes(searchFilter.toLowerCase()))
            : assets
          ).filter((asset) => owned ? asset.balance > 0 : true).map((asset) => {
            return (
              <tr
                key={asset.name}
                sx={{
                  '&:hover': {
                    background: colors.blue_pigment
                  },
                }}
                onClick={() => {
                  setSelectedAsset(asset)
                  if (asset.name != selectedAsset?.name) {
                    setAssetAmount(0);
                  }
                  onOpen();
                }}
              >

                <td>
                  <div
                    style={{
                      alignItems: "center",
                      gap: "1em",
                    }}
                  >
                    <TokenAvatar imageSize={48} name={asset.name} />
                    {asset.name}
                  </div>
                </td>
                <td>
                  <div style={{ lineHeight: '3em', alignItems: 'center' }}>{(asset.balance / Math.pow(10, asset.digits)).toFixed(asset.digits)}</div>{" "}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>


    </Card>
  );
}
