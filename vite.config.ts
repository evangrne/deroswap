import { build, defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import { viteSingleFile } from "vite-plugin-singlefile";

// https://vitejs.dev/config/
export default defineConfig((config) => {
  return {
    plugins: [react(), viteSingleFile({ removeViteModuleLoader: true })],
    build: { outDir: "dist/" },
  };
});
