//@ts-ignore
import DeroBridgeApi from "dero-rpc-bridge-api";
import { toast } from "react-toastify";
import { AppState, ConnectionAction, ConnectionStatus, Entity } from "./types";
import {
  PieSwapData,
  PieSwapPairDataType,
  UserBalanceListType,
  UserData,
} from "./utils/PieswapDataContext";

// Initialize api
export function initBridgeApi(
  bridge: DeroBridgeApi,
  dispatch: React.Dispatch<ConnectionAction>
) {
  bridge.init().then(() => {
    dispatch(ConnectionAction.InitialisedConnection);
  });
}

const connectionCheckMethod = {
  [Entity.Wallet]: "get-address",
  [Entity.Daemon]: "get-info",
};

export interface IFailed {
  daemon: {
    value: number;
    increment: () => void;
    reset: () => void;
  };
  wallet: {
    value: number;
    increment: () => void;
    reset: () => void;
  };
}

// Function that check connection with daemon and then with wallet
export function checkConnectionWithDaemonAndWallet(
  state: AppState,
  dispatch: React.Dispatch<ConnectionAction>,
  failed: IFailed
) {
  if (state.bridge.initialized) {
    checkConnectionFor(Entity.Daemon).then(() => {
      checkConnectionFor(Entity.Wallet);
    });
  }

  async function checkConnectionFor(entity: Entity) {
    // use appropriate method for the entity
    const method = connectionCheckMethod[entity];

    // call the entity with the appropriate method
    await state.bridge[entity](connectionCheckMethod[entity])
      // if successful
      .then((response: any) => {
        // but error code is returned
        if (response.data?.error) {
          console.error(
            response.data.error.code,
            response.data.error.data,
            response.data.error.message
          );
          // count the number of failed attemps to avoid bloating the UI with error toasts
          failed[entity].increment();
          // Toast only once
          if (failed[entity].value < 1)
            // Toast
            toast.error(
              `${entity}: something went wrong. Please check your configuration`
            );
          // select appropriate action to update state, we either lost the wallet or the daemon
          const action =
            entity == Entity.Daemon
              ? ConnectionAction.LostDaemon
              : ConnectionAction.LostWallet;
          if (state[entity] != ConnectionStatus.Failed) {
            console.warn(state[entity], ConnectionStatus.Failed);

            // dispatch the information
            dispatch(action);
          }
        } else {
          // no error code
          // select appropriate succesful action
          const action =
            entity == Entity.Daemon
              ? ConnectionAction.HasDaemon
              : ConnectionAction.HasWallet;
          // reset the counter for this entity
          failed[entity].reset();

          // If we recover from a previously failed connection
          if (state[entity] == ConnectionStatus.Failed) {
            state.bridge = new DeroBridgeApi();
            setTimeout(() => {
              console.log("[api] recovered from previously failed state");
              toast.clearWaitingQueue();
              toast.success("Connected");

              initBridgeApi(state.bridge, dispatch);
            }, 100);
          }

          // dispatch the information
          if (state[entity] != ConnectionStatus.Ready) {
            // dispatch the information
            dispatch(action);
          }
        }
      })
      .catch((err: any) => {
        // carching an error
        console.error(err); // log it
        failed[entity].increment(); // increment failed connection attempt
        if (failed[entity].value < 1)
          // toast once
          toast.error(`${entity}: ${err.message}`); // toast
        // choose action
        const action =
          entity == Entity.Daemon
            ? ConnectionAction.LostDaemon
            : ConnectionAction.LostWallet;
        // dispatch the information
        if (state[entity] != ConnectionStatus.Failed) {
          // dispatch the information
          dispatch(action);
        }
      });
  }
}

// Gets a random address from the daemon
export async function getRandomAddress(
  deroBridgeApi: DeroBridgeApi
): Promise<string> {
  console.log("[api] getting destination...");
  const randomAddress = (await deroBridgeApi.daemon("get-random-address", {}))
    .data?.result?.address[0];
  return randomAddress;
}

// Get gas estimates for a swap operation
export async function scGetSwapGasEstimate(
  assetFrom: string,
  assetTo: string,
  valueFrom: number,
  deroBridgeApi: DeroBridgeApi,
  pairContract: string
) {
  // Get a random address
  const randomAddress = await getRandomAddress(deroBridgeApi);
  console.log("[api] getting wallet addess...");

  // get the signer's wallet address
  const walletAddress = (await deroBridgeApi.wallet("get-address", {})).data
    ?.result?.address;

  // build params
  console.log("[api] getting gas estimate...");
  const params = {
    sc_rpc: [
      { name: "SC_ACTION", datatype: "U", value: 0 },
      { name: "SC_ID", datatype: "H", value: pairContract },
      { name: "entrypoint", datatype: "S", value: "Swap" },
    ],
    transfers: [
      { scid: assetFrom, destination: randomAddress, burn: valueFrom },
      //{ scid: assetTo, destination: randomAddress, burn: 0 },
    ],
    signer: walletAddress,
  };
  console.log("Params", params);

  // Call get gas estimate
  const res = await deroBridgeApi.daemon("get-gas-estimate", params);

  console.log(res);

  // Return storage gas fee estimate only
  const gasEstimate = res.data.result.gasstorage;
  return gasEstimate;
}

export async function scSwap(
  assetFrom: string,
  assetTo: string,
  valueFrom: number,
  deroBridgeApi: DeroBridgeApi,
  pairContract: string
) {
  const randomAddress = await getRandomAddress(deroBridgeApi);
  const fees = await scGetSwapGasEstimate(
    assetFrom,
    assetTo,
    valueFrom,
    deroBridgeApi,
    pairContract
  );
  console.log("[api] start transfers...");

  // Build params
  const params: any = {
    scid: pairContract,
    ringsize: 2,
    sc_rpc: [{ name: "entrypoint", datatype: "S", value: "Swap" }],
    transfers: [
      { scid: assetFrom, destination: randomAddress, burn: valueFrom },
      //{ scid: assetTo, destination: randomAddress, burn: 0 },
    ],
    fees,
  };
  console.log("Params", params);

  return deroBridgeApi.wallet("start-transfer", params);
}

export async function scGetAddGasEstimate(
  assetFrom: string,
  assetTo: string,
  value: { valueFrom: number; valueTo: number },
  deroBridgeApi: DeroBridgeApi,
  pairContract: string
) {
  if (value.valueFrom == 0) {
    return "invalid";
  }
  const randomAddress = await getRandomAddress(deroBridgeApi);
  console.log("[api] getting wallet addess...");
  const walletAddress = (await deroBridgeApi.wallet("get-address", {})).data
    ?.result?.address;
  console.log("[api] getting gas estimate...");
  const params = {
    sc_rpc: [
      { name: "SC_ACTION", datatype: "U", value: 0 },
      { name: "SC_ID", datatype: "H", value: pairContract },
      { name: "entrypoint", datatype: "S", value: "AddLiquidity" },
    ],
    transfers: [
      { scid: assetFrom, destination: randomAddress, burn: value.valueFrom },
      { scid: assetTo, destination: randomAddress, burn: value.valueTo },
    ],
    signer: walletAddress,
  };

  console.log("Params", params);

  // Call get gas estimate
  try {
    const res = await deroBridgeApi.daemon("get-gas-estimate", params);
    console.log(res);
    if (res.data.error !== undefined) {
      return "error";
    }
    // Return storage gas fee estimate only
    const gasEstimate = res.data.result.gasstorage;
    return gasEstimate;
  } catch (error) {
    return "error";
  }
}

export async function scAdd(
  assetFrom: string,
  assetTo: string,
  valueFrom: number,
  valueTo: number,
  deroBridgeApi: DeroBridgeApi,
  pairContract: string
) {
  const randomAddress = await getRandomAddress(deroBridgeApi);

  const fees = await scGetAddGasEstimate(
    assetFrom,
    assetTo,
    { valueFrom, valueTo },
    deroBridgeApi,
    pairContract
  );
  console.log("[api] start transfers (add)...");

  const params = {
    scid: pairContract,
    ringsize: 2,
    sc_rpc: [{ name: "entrypoint", datatype: "S", value: "AddLiquidity" }],
    transfers: [
      {
        scid: assetFrom,
        destination: randomAddress,
        burn: valueFrom,
      },
      {
        scid: assetTo,
        destination: randomAddress,
        burn: valueTo,
      },
    ],
    fees,
  };
  console.log("Params", params);
  return deroBridgeApi.wallet("start-transfer", params);
}

export async function scGetRemoveGasEstimate(
  pairData: PieSwapPairDataType,
  userData: UserData,
  percent: number,
  deroBridgeApi: DeroBridgeApi
) {
  if (pairData.pairContract === undefined) {
    throw "[api] scGetRemoveGasEstimate Error: pairData undefined";
  }

  const pairContract = pairData.registry.contract;
  
  const randomAddress = await getRandomAddress(deroBridgeApi);
  
  
  console.log("[api] getting wallet addess...");
  const walletAddress = (await deroBridgeApi.wallet("get-address", {})).data
    ?.result?.address;

  console.log("[api] getting gas estimate...");
  const removeShares = Math.floor(
    (userData.pairs[`${pairData.registry.name1}:${pairData.registry.name2}`]
      .balance *
      percent) /
    100
  );

  const params = {
    sc_rpc: [
      { name: "SC_ACTION", datatype: "U", value: 0 },
      { name: "SC_ID", datatype: "H", value: pairContract },
      { name: "entrypoint", datatype: "S", value: "RemoveLiquidity" },
    ],
    transfers: [
      { scid: pairContract, destination: randomAddress, burn: removeShares },
    ],
    signer: walletAddress,
  };
  console.log("Params", params);

  // Call get gas estimate
  try {
    const res = await deroBridgeApi.daemon("get-gas-estimate", params);
    console.log(res);
    if (res.data.error !== undefined) {
      return "error";
    }
    // Return storage gas fee estimate only
    const gasEstimate = res.data.result.gasstorage;
    return gasEstimate;
  } catch (error) {
    return "error";
  }
}

export async function scRemove(
  pairData: PieSwapPairDataType,
  userData: UserData,
  percent: number,
  deroBridgeApi: DeroBridgeApi
) {
  const randomAddress = await getRandomAddress(deroBridgeApi);

  const fees = await scGetRemoveGasEstimate(pairData, userData, percent, deroBridgeApi);

  if (pairData === undefined || pairData.pairContract === undefined) {
    throw "[api] scRemove pairData[.pairContract] is undefined";
  } else {
    console.log("[api] start transfers (add)...");

    const removeShares = Math.floor(
      (userData.pairs[`${pairData.registry.name1}:${pairData.registry.name2}`]
        .balance *
        percent) /
      100
    );

    const params = {
      scid: pairData.registry.contract,
      ringsize: 2,
      sc_rpc: [{ name: "entrypoint", datatype: "S", value: "RemoveLiquidity" }],
      transfers: [
        {
          scid: pairData.registry.contract,
          destination: randomAddress,
          burn: removeShares,
        },
      ],
      fees,
    };
    console.log("Params", params);

    return deroBridgeApi.wallet("start-transfer", params);
  }
}

export async function scReverseBridgeGasEstimate(
  scid: string,
  amount: number,
  ethAddress: string,
  bridgeFee: number,
  deroBridgeApi: DeroBridgeApi
) {

  const randomAddress = await getRandomAddress(deroBridgeApi);
  console.log("[api] getting wallet addess...");

  const walletAddress = (await deroBridgeApi.wallet("get-address", {})).data
    ?.result?.address;

  console.log("[api] getting gas estimate...");
  const deroAsset = "0000000000000000000000000000000000000000000000000000000000000000";
  const params = {
    sc_rpc: [
      { name: "SC_ACTION", datatype: "U", value: 0 },
      { name: "SC_ID", datatype: "H", value: scid },
      { name: "entrypoint", datatype: "S", value: "Bridge" },
      { name: "eth_addr", datatype: "S", value: ethAddress },
    ],

    transfers: [
      { scid: scid, destination: randomAddress, burn: amount },
      { scid: deroAsset, destination: randomAddress, burn: bridgeFee },
    ],
    signer: walletAddress,
  };
  console.log("Params", params);

  // Call get gas estimate
  try {
    const res = await deroBridgeApi.daemon("get-gas-estimate", params);
    console.log(res);
    if (res.data.error !== undefined) {
      return "error";
    }
    // Return storage gas fee estimate only
    const gasEstimate = res.data.result.gasstorage;
    return gasEstimate;
  } catch (error) {
    return "error";
  }
}

export async function scReverseBridge(
  scid: string,
  amount: number,
  ethAddress: string,
  bridgeFee: number,
  deroBridgeApi: DeroBridgeApi
) {
  const randomAddress = await getRandomAddress(deroBridgeApi);
  const deroAsset = "0000000000000000000000000000000000000000000000000000000000000000";

  const fees = await scReverseBridgeGasEstimate(scid, amount, ethAddress, bridgeFee, deroBridgeApi);
  console.log("[api] start transfers (reverse bridge)...");

  const params: any = {
    scid: scid,
    ringsize: 2,
    sc_rpc: [
      { name: "entrypoint", datatype: "S", value: "Bridge" },
      { name: "eth_addr", datatype: "S", value: ethAddress },
    ],
    transfers: [
      { scid: scid, destination: randomAddress, burn: amount },
      { scid: deroAsset, destination: randomAddress, burn: bridgeFee },
    ],
    fees,
  };
  console.log("Params", params);

  return deroBridgeApi.wallet("start-transfer", params);
  
}


export async function scAssetTransfer(
  scid: string,
  amount: number,
  destination: string,
  deroBridgeApi: DeroBridgeApi
) {
  const params = {
    ringsize: 2, //! TODO Remove or set higher when https://github.com/deroproject/derohe/issues/100 is fixed
    transfers: [
      { scid, destination, amount: amount },
    ]
  };
  console.log("Params", params);

  return deroBridgeApi.wallet("start-transfer", params);
  
}



