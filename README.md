# Dero DeX

## What it is
A decentralized exchange application based on DERO ([derohe](https://github.com/deroproject/derohe)). This repo holds the front-end code.


## Features

* Check your **asset balances** (for assets that have a pool)
* **Transfer assets** to other wallets
* **Swap** between different assets
* **Add** and **remove liquidity** from pools
* **Pool history**
* **Bridge** ERC20 asset from and to Ethereum

>
> [**derodex** on *IPFS* 🌐](ipns://k51qzi5uqu5dlmeekhaokr511s5giok8vxwudv2090cx4rgg4ab4okl3uzfquh)
>

If you do not want or know how to use *IPFS* :
>
> [**derodex** (hosted)](https://derodex.net/)
>
> [**derodex** on *IPFS* 🌐 (through a gateway)](https://ipfs.io/ipns/k51qzi5uqu5dlmeekhaokr511s5giok8vxwudv2090cx4rgg4ab4okl3uzfquh/)
>
---
# Roadmap

- [x] Swap
- [x] Asset balance list
- [x] [New] Asset transfer
- [x] Add/Remove liquidity
- [x] ERC20 Bridge from ethereum
- [x] ERC20 Bridge back to ethereum
- [ ] Transaction History
- [x] [New] Charts
- [ ] Vote

---
# Contribute
## Frontend stack

* Vite
* React.js
* Typescript
* dero-rpc-bridge-api
* ethers.js
* Theme UI

## Start the dev server

```py
# install and launch dev server
yarn && yarn dev
```
