import { Image } from "theme-ui";

import { logo } from "../assets/base64logo";

export default function Bridge() {
    return <div style={{flexDirection: "column", justifyContent: "center", alignItems: "center", width: "100%"}}>
      <Image src={logo} width="200px" height='200px'/>
      <div>Welcome to Dero DeX, swap freely and privately!</div>
    </div>;
  }
  