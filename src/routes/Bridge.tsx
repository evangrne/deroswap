import { useCallback, useContext, useEffect, useState } from "react";
import { Card } from "../components/content/Card";
import { ethers } from "ethers";
import { bridgeAbi, tokenAbi } from "../utils/erc20bridge";
import { toast } from "react-toastify";
import { selectorInputStyles } from "../components/content/AmountSelector";
import { PieSwapDataContext, UserDataContext } from "../utils/PieswapDataContext";
import { colors } from "../theme";
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
import { TokenAvatar } from "../components/content/TokenAvatar";
import { toCamelCase } from "../utils/misc";
import { scReverseBridge, scReverseBridgeGasEstimate } from "../api";
import { DeroBridgeApiContext } from "../utils/DeroBridgeApiContext";

export default function Bridge() {
  const ethTokenAvatar = <TokenAvatar name="DWETH" imageSize={24} />
  const deroTokenAvatar = <TokenAvatar name="DERO" imageSize={24} />
  const tabHeaders = [
    <div style={{ gap: '.2em', alignItems: 'center' }}>{ethTokenAvatar} Ethereum → {deroTokenAvatar} Stargate</div>,
    <div style={{ gap: '.2em', alignItems: 'center' }}>{deroTokenAvatar} Stargate → {ethTokenAvatar} Ethereum</div>,
  ];
  const [tabIndex, setTabIndex] = useState(0);

  return <Card title="bridge" width="480px">
    <Tabs
      style={{ flexDirection: "column", width: "100%" }}
      onSelect={setTabIndex}>
      <TabList
        style={{
          display: "flex",
          userSelect: "none",
          padding: "0",
          width: "100%",
          alignContent: "flex-start",
          borderBottom: "1px solid " + colors.whiteAlpha(200),
        }}
      >
        {tabHeaders.map((tabName, index) => {
          return <Tab key={index} style={{
            display: "inline-block",
            borderRadius: ".5em .5em 0 0 ",
            padding: "1em",
            background:
              tabIndex == index
                ? colors.violet_blue_crayola
                : colors.whiteAlpha(100),
          }}>{tabName}</Tab>
        }
        )}
      </TabList>
      <TabPanel style={{ justifyContent: "flex-start", padding: '2rem' }}>
        <EthereumToStargateBridge />
      </TabPanel>
      <TabPanel>
        <StargateToEthereumBridge />
      </TabPanel>
    </Tabs>
  </Card>
}

// Common ethereum related states and effects
function useEthereum() {
  const [provider, setProvider] = useState<ethers.providers.Web3Provider | null>(null);
  const [signer, setSigner] = useState<ethers.providers.JsonRpcSigner | null>(null);
  const [account, setAccount] = useState<string | null>(null);
  const [balance, setBalance] = useState<ethers.BigNumber | null>(null);
  const [connected, setConnected] = useState(false);


  // Connect callback used when clicking button or landing in the page
  const connect = useCallback(async () => {
    console.log("connecting", { provider, connected });

    if (!connected && provider) {
      const _accounts = await provider.send("eth_requestAccounts", []);

      setConnected(true);
      setAccount(ethers.utils.getAddress(_accounts[0]));
      const _balance = await provider.getBalance(_accounts[0])
      setBalance(_balance);
    }
  }, [connected, provider])

  // Set provider when metamask available
  useEffect(() => {
    if ((window as any).ethereum !== undefined) {
      const _provider = new ethers.providers.Web3Provider((window as any).ethereum)
      setProvider(_provider);
      const _signer = _provider.getSigner();
      setSigner(_signer);
    }
  }, [(window as any).ethereum])

  // Ask for account info when provider is available
  useEffect(() => {
    if (provider) {
      connect();
    }
  }, [provider])
  return {
    provider, signer, account, balance, connected,
    setProvider, setSigner, setAccount, setBalance, setConnected,
    connect
  }
}


// Brige from stargate to ethereum
function EthereumToStargateBridge() {
  const { provider, signer, account, balance, connected,
    connect } = useEthereum()


  const [bridgeContract, setBridgeContract] = useState<{ contract: ethers.Contract, fee: ethers.BigNumber } | null>(null);
  const [userBridgeContract, setUserBridgeContract] = useState<{ contract: ethers.Contract, fee: ethers.BigNumber } | null>(null);
  const [tokenContract, setTokenContract] = useState<{ contract: ethers.Contract, fee?: ethers.BigNumber } | null>(null);


  const [symbols, setSymbols] = useState<string[]>([]);
  const [selectedSymbol, setSelectedSymbol] = useState<string | null>(null);

  const [checkingContract, setCheckingContract] = useState(false);

  const [tokenBalance, setTokenBalance] = useState<{ balance: ethers.BigNumber, digits: number, allowance: ethers.BigNumber } | null>(null);

  const [amount, setAmount] = useState(0);

  const [bridgingToken, setBridgingToken] = useState(false);

  const { data } = useContext(PieSwapDataContext);
  const { user } = useContext(UserDataContext);

  const [deroWallet, setDeroWallet] = useState(user.address);


  const [bridgeAddress, setBridgeAddress] = useState<string | null>(null);

  useEffect(() => {
    setDeroWallet(user.address)

  }, [user.address])

  // Retrieve dex.erc20 eth contract address from keystore
  useEffect(() => {
    if (data.metadata.keys) {
      const address = data.metadata.keys['dex.bridge.erc20'];
      if (address) {
        console.log({ address });

        setBridgeAddress(address);
      }
    }
  }, [data.metadata.keys])


  // Get bridge fees when provider is available
  useEffect(() => {
    if (provider && bridgeAddress) {
      const getSC = async () => {
        console.log({ bridgeAddress });

        const bc = new ethers.Contract(bridgeAddress, bridgeAbi, provider);

        const bridgeFee = await bc.bridgeFee();
        console.log({ bridgeFee });

        setBridgeContract({ contract: bc, fee: bridgeFee });
      }
      getSC();
    }
  }, [provider, bridgeAddress])


  // Get user space contract
  useEffect(() => {
    if (signer && bridgeAddress) {
      const getSC = async () => {
        const bc = new ethers.Contract(bridgeAddress, bridgeAbi, signer);
        const bridgeFee = await bc.bridgeFee();
        setUserBridgeContract({ contract: bc, fee: bridgeFee })
      }
      getSC()
    }
  }, [signer, bridgeAddress])

  // Get symbols
  useEffect(() => {
    if (signer && userBridgeContract) {
      const getSymbols = async () => {
        const numSymbols = await userBridgeContract.contract.getSymbolListLength();
        let _symbols = [];
        for (let index = 0; index < numSymbols; index++) {
          const symbol = await userBridgeContract.contract.symbolList(index);
          _symbols.push(symbol);
        }
        setSymbols(_symbols);
      }
      getSymbols()
    }
  }, [userBridgeContract])

  // Get token contract, balance, decimal and allowance when selected symbol changes
  useEffect(() => {
    if (provider && selectedSymbol && bridgeContract && bridgeAddress) {
      setCheckingContract(true);
      setTokenBalance(null);

      const checkContract = async () => {
        const ta = await bridgeContract.contract.registeredSymbol(selectedSymbol);
        const tc = new ethers.Contract(ta, tokenAbi, provider);
        setTokenContract({ contract: tc });

        const tokenBalance = await tc.balanceOf(account);
        const tokenDecimals = await tc.decimals();
        const tokenAllowance = await tc.allowance(account, bridgeAddress);

        setTokenBalance({ balance: tokenBalance, digits: tokenDecimals, allowance: tokenAllowance });

        setCheckingContract(false);
      }
      checkContract();
    }
  }, [selectedSymbol, bridgeAddress]);

  // Bridge Token button callback
  const bridgeToken = useCallback(async () => {

    if (tokenBalance && bridgeContract && tokenContract && signer && userBridgeContract) {

      // Parse amount
      const _amount = ethers.utils.parseUnits(amount.toString(), tokenBalance.digits);

      const ta = await bridgeContract.contract.registeredSymbol(selectedSymbol);
      const userTokenContract = new ethers.Contract(ta, tokenAbi, signer);

      // Check balance greater that amount
      if (tokenBalance.balance.gte(_amount)) {

        // Check allowance 
        if (_amount.gt(tokenBalance.allowance)) {
          // Transaction
          const txn1 = await (async () => {
            try {
              const tx1 = await userTokenContract.approve(bridgeAddress, ethers.utils.parseEther("1000000000"));
              toast.warn("Approve pending. Please wait...")
              // const tx1 = await userTokenContract.approve(bridgeAddress, ethers.utils.parseEther(amount.toString()));
              const tx2 = await tx1.wait();

              return;
            } catch (error: any) {
              console.error("Approve error", error);
              toast.error(`Error while approving : ${error}`);
            }
          })();

        }

        // Bridge transaction
        const options = { value: bridgeContract.fee }
        const txn2 = await (async () => {
          try {
            await userBridgeContract.contract.bridgeETH2DERO(ta, deroWallet, _amount, options);
            toast.success('Bridging successful. You can check the Assets page, your wrapped tokens should appear in a couple of minutes.')
          } catch (error: any) {
            console.error("bridgeETH2DERO error", error);
            toast.error(`Error while bridging : ${error}`);
          }
        })();

      } else {
        toast.error("Insufficient Balance.");
      }
    }
  }, [tokenBalance, bridgeAddress, bridgeContract, userBridgeContract, amount, signer, deroWallet])


  const defaultSelectValue = 'Select Symbol...';

  const tokenBalanceDisplay = (() => {
    if (tokenBalance) {
      let balance = null;
      try {
        balance = ethers.utils.formatUnits(tokenBalance.balance, tokenBalance.digits);
      } catch (err) {
        console.error(err);
      }
      return balance
    }
    return null;
  })();


  const balanceDisplay = ethers.utils.formatUnits((balance || 0).toString(), 18);
  const bridgeFeeDisplay = ethers.utils.formatUnits((bridgeContract?.fee || 0).toString(), 18)

  const disabled = checkingContract || bridgingToken || amount == 0 || deroWallet.length != 66 || Number(tokenBalanceDisplay) < amount

  // ------ Component
  return <div
    style={{ flexDirection: 'column', gap: '1.5rem', alignItems: 'flex-start', margin: '2rem 0', width: '100%' }}
  >
    {connected ? (<>
      <div style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
        <div style={{ gap: '1em' }}><b>ETH Account:</b> {account}</div>
        <div style={{ gap: '1em' }}><b>ETH Balance:</b> {balanceDisplay} <i>ETH</i></div>
      </div>
      <div style={{ gap: '1em' }}><b>Bridge Fee:</b>{bridgeFeeDisplay} <i>ETH</i></div>

      <select disabled={symbols.length == 0} onChange={(event) => {
        if (event.target.value == defaultSelectValue) {
          setSelectedSymbol(null);
        } else {
          setSelectedSymbol(event.target.value);
        }
      }}>
        <option>{defaultSelectValue}</option>
        {symbols.map((symbol) => {
          return <option key={symbol}>{symbol}</option>
        })}
      </select>

      {selectedSymbol && tokenBalance ? <>

        <div style={{ gap: '1em' }}><b>Token Balance:</b> {tokenBalanceDisplay?.toString()} <i>{selectedSymbol}</i></div>

        <div style={{ gap: '1.5em', width: '100%', justifyContent: 'flex-start', alignItems: 'center' }}>
          <label style={{ minWidth: '10rem' }}><b>Token Amount</b></label>
          <div>
            <input type="number" min='0' max={(tokenBalanceDisplay == null ? '0' : tokenBalanceDisplay)}
              value={amount}
              onChange={(event) => { setAmount(Number(event.target.value)) }}
              style={{
                ...selectorInputStyles, paddingLeft: '1em', width: '10em',
                background: (Number(tokenBalanceDisplay) >= amount ? selectorInputStyles.background : colors.purple_pizzazz)
              }} />
            <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900) }} onClick={() => { setAmount(Number(tokenBalanceDisplay) * .25) }}>25%</button>
            <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900) }} onClick={() => { setAmount(Number(tokenBalanceDisplay) * .5) }}>50%</button>
            <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900) }} onClick={() => { setAmount(Number(tokenBalanceDisplay) * .75) }}>75%</button>
            <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900) }} onClick={() => { setAmount(Number(tokenBalanceDisplay)) }}>max</button>
          </div>
        </div>


        <div style={{ gap: '1.5em', width: '100%', justifyContent: 'flex-start', alignItems: 'center' }}>
          <label style={{ minWidth: '10rem' }}><b>DERO wallet address</b></label>
          <input style={{ ...selectorInputStyles, paddingLeft: '1em', fontSize: '10pt', border: deroWallet.length == 66 ? 'inherit' : '2px solid ' + colors.purple_pizzazz }} onChange={(event) => { setDeroWallet(event.target.value) }} value={deroWallet} />
        </div>
        <div style={{ width: '100%' }}>
          <button disabled={disabled} onClick={bridgeToken} style={{
            backgroundColor: disabled ? colors.whiteAlpha(200) : colors.sky_blue_crayola,
            color: disabled ? colors.whiteAlpha(400) : colors.blackAlpha(900)
          }}>Bridge Token</button>
        </div>


      </> : null}
    </>) : <button onClick={connect} style={{ backgroundColor: colors.whiteAlpha(200), color: colors.whiteAlpha(900) }}>Connect</button>}


  </div>
}



// Reverse
function StargateToEthereumBridge() {
  const { account, connected,
    connect } = useEthereum()

  const [ethAccount, setEthAccount] = useState('');
  const [amount, setAmount] = useState(0);
  const [digits, setDigits] = useState(18);

  const [gasFees, setGasFees] = useState(0);

  const { state } = useContext(DeroBridgeApiContext);
  const { data } = useContext(PieSwapDataContext);
  const { user } = useContext(UserDataContext);

  const defaultSelectValue = 'Select Symbol...';

  const [selectedSymbol, setSelectedSymbol] = useState<string | null>(null);
  const selectedBalance = user.balances.find(balance => balance.name == selectedSymbol) || null;

  const tokenBalanceDisplay = selectedBalance ? (selectedBalance.balance / Math.pow(10, selectedBalance.digits)).toFixed(selectedBalance.digits) : null;

  const [reverseBridgeTokens, setReverseBridgeTokens] = useState<string[]>([])


  // Ethereum account
  useEffect(() => {
    if (account) {
      setEthAccount(account);
    }
  }, [account])

  // Symbols
  useEffect(() => {
    const bridgeableTokens = Object.keys(data.bridge.tokens);
    if (bridgeableTokens.length > 0) {
      setReverseBridgeTokens(bridgeableTokens);
      console.log({ bridgeableTokens });
    }
  }, [data.bridge.tokens])

  // Digits 
  useEffect(() => {
    if (selectedSymbol) {
      setDigits(Object.keys(data.pairs)
        .filter(p => p.includes(selectedSymbol))
        .map(p => data.pairs[p].registry.name1 == selectedSymbol ?
          data.pairs[p].registry.digits1 : data.pairs[p].registry.digits2)[0]);
    }
  }, [data.pairs, selectedSymbol])

  useEffect(() => {
    if (selectedSymbol && amount && ethers.utils.isAddress(ethAccount)) {
      const compute = async () => {
        const result = await scReverseBridgeGasEstimate(
          data.bridge.tokens[selectedSymbol].scid,
          Math.floor(amount * Number(Math.pow(10, digits))),
          ethAccount,
          data.bridge.fee,
          state.bridge,
        );
        setGasFees(result);
      };
      compute();
    }
  }, [selectedSymbol, amount, ethAccount])


  const reverseBridgeToken = async () => {
    if (selectedSymbol && !disabled) {
      console.log({
        args: {
          scid: data.bridge.tokens[selectedSymbol].scid,
          amount: Math.floor(amount * Number(Math.pow(10, digits))),
          ethAddress: ethAccount,
        },
        data,
        digits
      });

      scReverseBridge(
        data.bridge.tokens[selectedSymbol].scid,
        Math.floor(amount * Number(Math.pow(10, digits))),
        ethAccount,
        data.bridge.fee,
        state.bridge,
      ).then((result) => {
        toast.success('Bridge successful!');
        console.log({ result });
        setSelectedSymbol(null);
        setAmount(0);
      }).catch((err) => {
        toast.error('Something went wrong while bridging: ' + err)
      })


    }
  }

  const disabled = !ethers.utils.isAddress(ethAccount) || amount == 0 || Number(tokenBalanceDisplay) < amount;

  return <div
    style={{ flexDirection: 'column', gap: '1.5rem', alignItems: 'flex-start', margin: '0 2rem', width: '100%' }}
  >

    <div style={{ flexDirection: 'column', gap: '1.5em', alignItems: 'flex-start', width: '100%' }}>
      <div style={{ gap: '0.5em', width: '100%', justifyContent: 'flex-start', alignItems: 'center' }}>
        <label style={{ minWidth: '10rem', textAlign: 'left' }}><b>ETH Account (CamelCase):</b></label>
        <input style={{ ...selectorInputStyles, paddingLeft: '1em', textDecoration: 'none', fontSize: '10pt', border: ethers.utils.isAddress(ethAccount) ? 'inherit' : '2px solid ' + colors.purple_pizzazz }} onChange={(event) => { setEthAccount(event.target.value) }} value={ethAccount} />
      </div>
      <select disabled={reverseBridgeTokens.length == 0} onChange={(event) => {
        setAmount(0);
        if (event.target.value == defaultSelectValue) {

          setSelectedSymbol(null);
        } else {
          setSelectedSymbol(event.target.value);
        }
      }}>
        <option>{defaultSelectValue}</option>
        {reverseBridgeTokens.map((symbol) => {
          return <option key={symbol}>{symbol}</option>
        })}
      </select>

      <div>
        <label style={{ minWidth: '10rem', textAlign: 'left' }}><b>Token Balance</b>: {tokenBalanceDisplay} {selectedSymbol}</label>
      </div>
      <div>
        <label style={{ minWidth: '10rem', textAlign: 'left' }}><b>Bridge Fee</b>: {(data.bridge.fee / Math.pow(10, 5)).toFixed(5)} DERO</label>
      </div>

      <div style={{ gap: '1.5em', width: '100%', justifyContent: 'flex-start', alignItems: 'center' }}>
        <label style={{ textAlign: "left", minWidth: '8rem' }}><b>Token Amount: </b></label>
        <div style={{ width: '100%', justifyContent: 'flex-start' }}>
          <input type="number" min='0' max={(tokenBalanceDisplay == null ? '0' : tokenBalanceDisplay)}
            value={amount}
            onChange={(event) => { setAmount(Number(Number(event.target.value).toFixed(digits))) }}
            style={{
              ...selectorInputStyles, paddingLeft: '1em', width: '10em',
              background: (Number(tokenBalanceDisplay) >= amount ? selectorInputStyles.background : colors.purple_pizzazz)
            }} />
          <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900) }} onClick={() => { setAmount(Number(tokenBalanceDisplay) * .25) }}>25%</button>
          <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900) }} onClick={() => { setAmount(Number(tokenBalanceDisplay) * .5) }}>50%</button>
          <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900) }} onClick={() => { setAmount(Number(tokenBalanceDisplay) * .75) }}>75%</button>
          <button style={{ background: colors.blackAlpha(700), color: colors.whiteAlpha(900) }} onClick={() => { setAmount(Number(tokenBalanceDisplay)) }}>max</button>
        </div>
      </div>
      <div>
        <label style={{ minWidth: '10rem', textAlign: 'left' }}><b>Gas Fees</b>: {gasFees > 0 ? <>{(gasFees / Math.pow(10, 5)).toFixed(5)} DERO</> : <></>}</label>
      </div>
      <div style={{ width: '100%' }}>
        <button disabled={disabled} onClick={reverseBridgeToken} style={{ background: disabled ? colors.whiteAlpha(200) : colors.sky_blue_crayola, color: disabled ? colors.whiteAlpha(300) : 'black' }}>Reverse Bridge Token</button>
      </div>

    </div>

  </div>
}
