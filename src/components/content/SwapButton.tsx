/** @jsxImportSource theme-ui */


import { useCallback, useContext, useEffect, useState } from "react";
import Modal from "react-modal";
import { toast } from "react-toastify";
import { ThemeUIStyleObject } from "theme-ui";
import { scGetSwapGasEstimate, scSwap } from "../../api";
import { colors, hexToRgba, defaultModalStyles, skew } from "../../theme";
import { StyleDefinition, SwapStateType } from "../../types";
import { DeroBridgeApiContext } from "../../utils/DeroBridgeApiContext";
import { findPair, useModal, wait } from "../../utils/misc";
import {
  getAssetsAndDigits,
  PieSwapData,
  PieSwapDataContext,
  PieSwapPairDataType,
} from "../../utils/PieswapDataContext";

type SwapButtonProps = {
  disabled: boolean;
  swapState: SwapStateType;
};

export function SwapButton({ disabled, swapState }: SwapButtonProps) {
  // Context loading
  const { data, updateData } = useContext(PieSwapDataContext);

  // State
  const { isOpen, onOpen, onClose } = useModal();
  const { state, update } = useContext(DeroBridgeApiContext);

  type FeesType = {
    amountToWithFees: number;
    poolFees: number;
    gasFees: number;
  };

  const [digits, setDigits] = useState({digitsFrom: 0, digitsTo: 0});

  
  // Compute the fees and the amount with fees applied
  async function computeAmountWithFees(
    swapState: SwapStateType,
    data: PieSwapData
  ): Promise<FeesType> {
    // Find the selected pair in the data
    const { pairData, reverse } = findPair(
      swapState.tokenFrom,
      swapState.tokenTo,
      data
    );

    // If it is defined
    if (pairData !== undefined && pairData.pairContract !== undefined) {
      // Compute fees and amounts
      const amountToWithFees =
        (swapState.amountTo * (10000 - pairData.pairContract?.poolFee)) / 10000;
      const poolFees =
        (swapState.amountTo * pairData.pairContract?.poolFee) / 10000;

      const { assetFrom, assetTo, digitsFrom, digitsTo } = getAssetsAndDigits(
        pairData,
        reverse
      );
      setDigits({digitsFrom, digitsTo})

      // Request gas fees
      const gasFees =
        (await scGetSwapGasEstimate(
          assetFrom,
          assetTo,
          Math.floor(swapState.amountFrom * Math.pow(10, digitsFrom)),
          state.bridge,
          pairData.registry.contract
        )) / 100000;

      return { amountToWithFees: Number(amountToWithFees.toFixed(digitsTo)), poolFees, gasFees };
    }

    return { amountToWithFees: 0, poolFees: 0, gasFees: 0 };
  }


  const [fees, setFees] = useState<null | FeesType>(null);

  // Compute and update fees when modal is open or data has changed
  useEffect(() => {
    if (isOpen) {
      computeAmountWithFees(swapState, data)
        .then(({ amountToWithFees, poolFees, gasFees }: FeesType) => {
          setFees({ amountToWithFees, poolFees, gasFees });
        })
        .catch((err) => {
          console.error(err);
          toast.error("something went wrong when computing gas fees...");
        });
    }
  }, [data, isOpen]);

  // When confirming swap
  const [isSwapping, setSwapping] = useState<"done" | boolean>(false);

  // Callback when starting the swap transaction
  const startSwapping = () => {
    const { pairData, reverse } = findPair(
      swapState.tokenFrom,
      swapState.tokenTo,
      data
    );
    // If pair is defined
    if (pairData !== undefined && pairData.pairContract !== undefined) {
      const { assetFrom, assetTo, digitsFrom } = getAssetsAndDigits(
        pairData,
        reverse
      );

      scSwap(
        assetFrom,
        assetTo,
        Math.floor(swapState.amountFrom * Math.pow(10, digitsFrom)),
        state.bridge,
        pairData.registry.contract
      )
        .then((result) => {
          console.log(result);
          toast.success("successfully swapped !");
          setSwapping("done");
        })
        .catch((err) => {
          setSwapping(false);
          toast.error("something went wrong while swapping");
          console.error(err);
        });
    }
  };

  const onModalRequestClose = useCallback(() => {
    if (isSwapping == false) {
      onClose();
    }
  }, [isSwapping]);

  // ---- Dynamic styles

  // Glow effect
  let swapDropShadowColor = hexToRgba(colors.sky_blue_crayola);
  swapDropShadowColor.a = 0.4;

  const filter = disabled
    ? "none"
    : `drop-shadow(0 0 0.75rem rgba(${swapDropShadowColor.r}, ${swapDropShadowColor.g}, ${swapDropShadowColor.b}, ${swapDropShadowColor.a}))`;
  const hover = disabled
    ? {}
    : {
        filter: `drop-shadow(0 0 0.75rem rgba(${swapDropShadowColor.r}, ${
          swapDropShadowColor.g
        }, ${swapDropShadowColor.b}, ${swapDropShadowColor.a * 2}))`,
      };
  // button style
  const buttonStyles: ThemeUIStyleObject = {
    margin: "1em 3em",
    color: colors.blackAlpha(900),
    background:
      disabled || isSwapping != false
        ? colors.whiteAlpha(400)
        : colors.sky_blue_crayola,
    transform: `skew(-${skew}deg)`,
    fontWeight: "bold",
    fontSize: "1.1em",
    userSelect: "none",
    borderRadius: "4px",
    transition: "filter .3s ease-in-out",
    filter: filter,
    "&:hover": hover,
  };

  const modalStyles: StyleDefinition = {
    layout: { flexDirection: "column", gap: "1em", fontFamily: "monospace" },
    body: {
      gap: "1em",
      alignItems: "center",
      flexDirection: "column",
      fontSize: ".9em",
    },
    title: { fontWeight: "bold", fontSize: "1.1em" },
    feeCard: {
      flexDirection: "column",
      background: colors.blackAlpha(100),
      padding: "1em",
      borderRadius: ".5em",
    },
  };


  // Modal Texts
  const exchangeText = (
    <>
      Exchange {Number(swapState.amountFrom).toFixed(digits.digitsFrom)} {swapState.tokenFrom} for{" "}
      {Number(fees?.amountToWithFees).toFixed(digits.digitsTo)} {swapState.tokenTo} ?
    </>
  );

  const poolFeesText = (
    <span>
      Pool fees : {Number(fees?.poolFees || 0).toFixed(digits.digitsTo)} {swapState.tokenTo}
    </span>
  );

  const gasFeesText = <span>Gas fees : {fees?.gasFees} DERO</span>;

  // Modal buttons
  const cancelButton =
    isSwapping != false ? (
      <></>
    ) : (
      <button style={{ background: "transparent", color: colors.whiteAlpha(900) }} onClick={onClose}>
        Cancel
      </button>
    );

    
  const confirmSwapButton = (
    <button
      style={{
        background:
          isSwapping == false
            ? colors.sky_blue_crayola
            : isSwapping == "done"
            ? colors.violet_blue_crayola
            : colors.battleship_grey,
        color: colors.blackAlpha(900),
      }}
      onClick={() => {
        switch (isSwapping) {
          case "done":
            setSwapping(false);
            onClose();
            break;
          case false:
            setSwapping(true);
            startSwapping();
            break;
          default:
            break;
        }
      }}
      disabled={isSwapping == true}
    >
      {isSwapping == true
        ? "swapping..."
        : isSwapping == "done"
        ? "Ok"
        : "Swap"}
    </button>
  );


  // ----------------- Modal
  const modal = (
    <Modal
      isOpen={isOpen}
      onAfterOpen={() => {}}
      onRequestClose={onModalRequestClose}
      style={defaultModalStyles}
    >
      <div style={modalStyles.layout as any}>
        <div style={modalStyles.body as any}>
          <label style={modalStyles.title as any}>Confirm Swap</label>
          <div>{exchangeText}</div>
          <div style={modalStyles.feeCard as any}>
            {poolFeesText}
            {gasFeesText}
          </div>
        </div>

        <div style={{ flexDirection: "row", justifyContent: "flex-end" }}>
          {cancelButton}
          {confirmSwapButton}
        </div>
      </div>
    </Modal>
  );

  // --------------- Component
  return (
    <>
      <button
        sx={buttonStyles}
        disabled={disabled || isSwapping == true}
        onClick={onOpen}
      >
        <div style={{ transform: `skew(${skew}deg)`, color: disabled || isSwapping == true ? colors.blackAlpha(400) : colors.blackAlpha(900)}}>SWAP</div>
      </button>
      {modal}
    </>
  );
}
