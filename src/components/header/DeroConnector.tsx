import { useContext, useEffect, useState } from "react";
import {
  initBridgeApi,
  checkConnectionWithDaemonAndWallet,

} from "../../api";

import {
  ConnectionStatus,
  Entity,
} from "../../types";
import { ImCheckmark, ImCross } from "react-icons/im";
import { DeroBridgeApiContext } from "../../utils/DeroBridgeApiContext";
import { PieSwapDataContext } from "../../utils/PieswapDataContext";
import { colors } from "../../theme";

// Dero connector manages the connection with the bridge api
export function DeroConnector(props: { width: string }) {
  // Context loading
  const { state, update } = useContext(DeroBridgeApiContext);
  const { data } = useContext(PieSwapDataContext);

  // Connection check time interval
  const [connectionCheckInterval, setConnectionCheckInterval] = useState(2000);

  const useCounter = () => {
    const [_f, _set_f] = useState(0);
    return {
      value: _f,
      increment: () => {
        _set_f(_f + 1);
      },
      reset: () => {
        _set_f(0);
      },
    };
  };

  // Failed state
  const failed = {
    [Entity.Daemon]: useCounter(),
    [Entity.Wallet]: useCounter(),
  };

  // Initialize bridge api on window loaded
  useEffect(() => {
    // Onload : init bridge api
    const load = () => {
      initBridgeApi(state.bridge, update);
    };
    // Add it when page is loaded
    window.addEventListener("load", load);
    // Remove it when effect is cancelled
    return () => window.removeEventListener("load", load);
  }, []);

  // Setup periodic connection check
  useEffect(() => {
    // Check once
    checkConnectionWithDaemonAndWallet(state, update, failed);

    // Create interval
    const interval = setInterval(() => {
      // Check again

      checkConnectionWithDaemonAndWallet(state, update, failed);
    }, connectionCheckInterval);

    
    // Clear interval when effect is removed
    return () => {
      clearInterval(interval);
    };
  }, [state, connectionCheckInterval]);


  // Dynamic Styles
  const connectionIcon = (entityState: ConnectionStatus) => {
    switch (entityState) {
      case ConnectionStatus.Connected:
        return <ImCheckmark />;
      case ConnectionStatus.NotConnected:
        return <ImCross color="lightgray" />;
      case ConnectionStatus.Failed:
        return <ImCross color="red" />;
      case ConnectionStatus.Ready:
        return <ImCheckmark color="lightgreen" />;
      default:
        return <ImCross color="lightgray" />;
    }
  };

  // Small vertical spacer
  const spacer = <span style={{ width: ".5em" }}></span>;
  
  //  ----------------- Component
  return (
    <div
      style={{ width: props.width, flexDirection: "row", flexWrap: 'wrap', gap: '2em' }}
    >
      <div style={{ lineHeight: "1.5em", alignItems: "center" }}>
        {connectionIcon(state.daemon)}
        {spacer}daemon
      </div>
      <div style={{ lineHeight: "1.5em", alignItems: "center" }}>

        {connectionIcon(state.wallet)}
        {spacer}wallet
      </div>
      <div style={{ lineHeight: "1.5em", alignItems: "center" }}>
        <div style={{
          background: colors.purple_pizzazz,
          padding: '.3em .4em',
          fontWeight: 'bold',
          borderRadius: "8px"
        }}>
          {data.metadata.network}
        </div>

      </div>

    </div>
  );
}
