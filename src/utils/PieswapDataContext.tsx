import {
  createContext,
  Dispatch,
  useCallback,
  useContext,
  useEffect,
  useReducer,
  useState,
} from "react";
import { toast } from "react-toastify";
import { ConnectionStatus, DataAction } from "../types";
import { DeroBridgeApiContext } from "./DeroBridgeApiContext";


export type PieSwapPairDataType = {
  registry: {
    name1: string;
    name2: string;
    contract: string;

    asset1: string;
    asset2: string;

    digits1: number;
    digits2: number;
  };
  pairContract?: {
    value1: number;
    value2: number;
    hasLiquidity: boolean;
    balance: number;
    shares: number;
    poolFee: number;
    ratio1to2: number;
  };

  triedLoadingContract: boolean;

  fiat?: {
    ratio1toFiat: number;
    ratio2toFiat: number;
  };
};

export type PieSwapData = {
  metadata: {
    pairs: {
      count: number;
      combinations: string[][];
    };
    tokens: {
      count: number;
      array: string[];
    };
    network: 'mainnet' | 'testnet' | null;
    keys: any;
  };
  pairs: {
    [pairName: string]: PieSwapPairDataType;
  };
  bridge: {
    tokens: {
      [tokenName: string]: {
        scid: string
      }
    };
    fee: number;
  }
};

type PieSwapDataContextType = {
  data: PieSwapData;
  updateData: Dispatch<ReducerData>;
};

const initialData: PieSwapData = {
  metadata: {
    pairs: { count: 0, combinations: [] },
    tokens: { count: 0, array: [] },
    network: null,
    keys: null,
  },
  pairs: {},
  bridge: {
    tokens: {},
    fee: 0,
  },
};

type GetSCRegistryResult = {
  stringkeys: {
    C: string;
    fee: number;
    numTrustees: number;

    pairs: number;
    quorum: number;

    tokens: number;
    version: string;

    [info: string]: string | number;
  };
};

type GetSCPairContractResult = {
  balance: number;
  stringkeys: {
    C: string;
    adds: number;
    asset1: string;
    asset2: string;
    decimals: number;
    fee: number;
    name: string;
    numTrustees: number;
    "o:...": number;
    "ol:0": string;
    quorum: number;
    rems: number;
    sharesOutstanding: number;
    swaps: number;
    symbol: string;
    val1: number;
    val2: number;
    version: string;
  };
};

type ReducerData = {
  action: DataAction;
  result?: GetSCRegistryResult | GetSCPairContractResult | any;
  pair?: string;
  network?: 'mainnet' | 'testnet' | null;
  keys?: any;
};

export type UserBalanceListType = {
  name: string;
  asset: string;
  balance: number;
  digits: number;
}[];

export type UserData = {
  balances: UserBalanceListType;

  pairs: {
    [pairName: string]: {
      balance: number;
    };
  };

  address: string;
};

const initialUserData: UserData = {
  balances: [],
  pairs: {},
  address: '',
};

export enum UserDataAction {
  UpdateAssetBalance = "UpdateAssetBalance",
  UpdatePairBalance = "UpdatePairBalance",
  UpdateDeroAddress = "UpdateDeroAddress",
  FetchAfterOperation = "UpdateAfterOperation",
}

type GetBalanceResult = any;

type UserReducerData = {
  action: UserDataAction;
  result: {
    name: string;
    content: GetBalanceResult;
  };
  scData?: PieSwapData;
};

type UserDataContextType = {
  user: UserData;
  updateUserData: Dispatch<UserReducerData>;
  fetchUserData: () => void;
};

function parseRegistryData(result: GetSCRegistryResult) {
  const rawPairs = Object.entries(result.stringkeys)
    .filter((entry) => {
      const [key, _] = entry;
      return key.startsWith("p:");
    })
    .map((entry) => {
      const [key, value] = entry;
      const [name1, name2] = key.substring(2).split(":");
      return { registry: { name1, name2, contract: String(value) } };
    });

  let assets: { [asset: string]: string } = {};
  Object.entries(result.stringkeys)
    .filter((entry) => {
      const [key, _] = entry;
      return key.startsWith("t:") && key.endsWith(":c");
    })
    .forEach((entry) => {
      const [key, value] = entry;
      const name = key.split(":")[1];
      assets[name] = String(value);
    });

  let digits: { [asset: string]: number } = {};
  Object.entries(result.stringkeys)
    .filter((entry) => {
      const [key, _] = entry;
      return key.startsWith("t:") && key.endsWith(":d");
    })
    .forEach((entry) => {
      const [key, value] = entry;
      const name = key.split(":")[1];
      const numValue = Number(value);
      digits[name] = Number.isNaN(numValue) ? -1 : numValue;
    });

  let pairs: any = {};
  rawPairs.forEach((rawPair) => {
    pairs[`${rawPair.registry.name1}:${rawPair.registry.name2}`] = {
      registry: {
        ...rawPair.registry,
        asset1: assets[rawPair.registry.name1],
        asset2: assets[rawPair.registry.name2],
        digits1: digits[rawPair.registry.name1],
        digits2: digits[rawPair.registry.name2],
      },
    };
  });

  const data = {
    metadata: {
      pairs: {
        count: result.stringkeys.pairs,
        combinations: rawPairs.flatMap((pair) => {
          return [
            [pair.registry.name1, pair.registry.name2],
            [pair.registry.name2, pair.registry.name1],
          ];
        }),
      },
      tokens: {
        array: Object.keys(assets),
        count: result.stringkeys.tokens,
      },
    },
    pairs: pairs,
  };

  return data;
}

function parseBridgeRegistryData(result: GetSCRegistryResult) {
  let tokens: any = {}
  Object.keys(result.stringkeys).filter(key => key[0] == 's').forEach(key => {
    tokens[key.split(':')[1]] = {
      scid: result.stringkeys[key]
    }
  })
  const fee = Number(result.stringkeys.bridgeFee);
  return { tokens, fee }
}

function parsePairData(
  data: PieSwapData,
  result: GetSCPairContractResult,
  pair: string
) {
  if (result.stringkeys === undefined) {
    toast.error("Something is wrong with the configuration of you daemon / wallet. Make sure they are both connected to the same network and reload the page.");
    return data;
  }
  let newData = { ...data };

  const [val1, val2, fee, shares] = [
    result.stringkeys.val1,
    result.stringkeys.val2,
    result.stringkeys.fee,
    result.stringkeys.sharesOutstanding,
    //result.stringkeys.balance,
  ];
  const [realVal1, realVal2] = [
    val1 / Math.pow(10, data.pairs[pair].registry.digits1),
    val2 / Math.pow(10, data.pairs[pair].registry.digits2),
  ];
  newData.pairs[pair].pairContract = {
    value1: val1,
    value2: val2,
    poolFee: fee,
    balance: result.balance,
    shares,
    hasLiquidity: val1 > 0 || val2 > 0,
    ratio1to2: realVal2 / realVal1,
  };
  return newData;
}

export function getAssetsAndDigits(
  pairData: PieSwapPairDataType,
  reverse: boolean
) {
  // Retrieve assets from registry
  const [assetFrom, assetTo] = [
    reverse ? pairData.registry.asset2 : pairData.registry.asset1,
    reverse ? pairData.registry.asset1 : pairData.registry.asset2,
  ];

  // Retrieve precisions from registy
  const [digitsFrom, digitsTo] = [
    reverse ? pairData.registry.digits2 : pairData.registry.digits1,
    reverse ? pairData.registry.digits1 : pairData.registry.digits2,
  ];

  return { assetFrom, assetTo, digitsFrom, digitsTo };
}

function reducer(
  data: PieSwapData,
  { action, result, pair, network, keys }: ReducerData
): PieSwapData {
  let parsed = null;
  switch (action) {
    case DataAction.UpdateNetwork:
      return { ...data, metadata: { ...data.metadata, network: (network || null) } }
    case DataAction.UpdateKeys:
      return { ...data, metadata: { ...data.metadata, keys: (keys || null) } }
    case DataAction.UpdateRegistry:
      parsed = parseRegistryData(result);

      return {
        ...parsed, // change in other keys
        metadata: {
          ...parsed.metadata, // change in metadata
          network: data.metadata.network,
          keys: data.metadata.keys,
        },
        bridge: data.bridge,
      };

    case DataAction.UpdateBridgeRegistry:
      parsed = parseBridgeRegistryData(result);
      return {
        pairs: data.pairs,
        metadata: data.metadata,
        bridge: { ...parsed },
      }

    case DataAction.UpdatePairContract:
      if (pair !== undefined && result !== undefined) {
        return parsePairData(data, result, pair);
      }
    default:
      console.warn("No change in the data", action);
      return data;
  }
}

function userReducer(
  data: UserData,
  { action, result, scData }: UserReducerData
): UserData {
  switch (action) {
    case UserDataAction.UpdateAssetBalance:
      // If data correctly passed
      if (scData !== undefined && scData.pairs !== undefined) {
        // Find existing in balances
        let existing = data.balances.find(({ name }) => name == result.name);

        // If one found
        if (existing !== undefined) {
          // Simply update the balance
          data.balances = data.balances.map((balanceData) => {
            if (balanceData.name == result.name) {
              return { ...balanceData, balance: result.content.balance };
            }
            return balanceData;
          });
          return { ...data, pairs: { ...data.pairs } };
        }

        // If none found find the asset data
        const assetPairKey = Object.keys(scData?.pairs || {}).find((pairName) =>
          pairName.includes(result.name)
        );
        if (assetPairKey === undefined) {
          console.error("asset not found in data for " + result.name);
        } else {
          const assetPair = scData.pairs[assetPairKey];
          const [asset, digits] =
            assetPair?.registry.name2 == result.name
              ? [assetPair.registry.asset2, assetPair.registry.digits2]
              : [assetPair.registry.asset1, assetPair.registry.digits1];

          data.balances.push({
            name: result.name,
            asset,
            digits,
            balance: result.content.balance,
          });
          return { ...data }
        }
      } else {
        console.error("contract data not provided to userReducer");
      }
      console.warn("Nothing changed in user data");
      return { ...data };

    case UserDataAction.UpdatePairBalance:
      // If data correctly passed
      if (scData !== undefined && scData.pairs !== undefined) {
        // Find existing in balances
        let existing = Object.keys(data.pairs).find(
          (name) => name == result.name
        );

        // If one found
        if (existing !== undefined) {
          // Simply update the balance
          data.pairs[existing].balance = result.content.balance;
          return { ...data, pairs: { ...data.pairs } };
        }

        // If none found find the asset data
        data.pairs[result.name] = { balance: result.content.balance };
        return { ...data };
      } else {
        console.error("contract data not provided to userReducer");
      }
      console.warn("Nothing changed in user data");
      return data;

    case UserDataAction.UpdateDeroAddress:
      return { ...data, address: result.content }
    default:
      console.warn("No change in user data.", action);
      return data;
  }
}

type GetBalanceResponse = {
  data: {
    id: string;
    jsonrpc: string;
    result: {
      balance: number;
      unlocked_balance: 0;
    };
    error?: { code: number, message: string }
  };
  
};

type GetSCResponse = {
  data: {
    id: string;
    jsonrpc: string;
    result: GetSCRegistryResult | GetSCPairContractResult;
  };
};

export const PieSwapDataContext = createContext<PieSwapDataContextType>({
  data: initialData,
  updateData: () => { },
});

export const UserDataContext = createContext<UserDataContextType>({
  user: initialUserData,
  updateUserData: () => { },
  fetchUserData: () => { },
});

// Component wrapper
export function PieSwapDataProvider(props: any) {
  const [data, updateData] = useReducer(reducer, initialData);
  const [userData, updateUserData] = useReducer(userReducer, initialUserData);

  const [waitTimes, _] = useState({
    beforeFetching: 20,
    betweenFetches: 80,
    periodicFetches: 10000,
  });

  const { state } = useContext(DeroBridgeApiContext);
  const ready =
    state.daemon == ConnectionStatus.Ready &&
    state.wallet == ConnectionStatus.Ready;

  const [pairContractsLoaded, setPairContractsLoaded] = useState(0);

  const [userDataFetches, setUserDataFetches] = useState(0);


  const fetchUserData = useCallback(() => {
    setUserDataFetches(Math.random());
  }, [userDataFetches])

  useEffect(() => {
    if (state.wallet == ConnectionStatus.Ready) {
      state.bridge.wallet('get-address').then((response: any) => {
        const deroAddress = response.data.result.address;
        updateUserData({
          action: UserDataAction.UpdateDeroAddress, result: { name: '', content: deroAddress }
        });
        console.log({ deroAddress });

      }).catch((err: any) => {
        console.error(err);
      })
    }
  }, [state.wallet])

  // Retrieve dex.erc20 eth contract address from keystore
  useEffect(() => {
    const timeout = setTimeout(() => {
      // If connection is established with the dero rpc api
      if (state.daemon == ConnectionStatus.Ready) {

        // Decode function for hex dump
        function decode(hex: any) {
          var bytes = [], str;

          for (var i = 0; i < hex.length - 1; i += 2)
            bytes.push(parseInt(hex.substr(i, 2), 16));

          return String.fromCharCode.apply(String, bytes);
        }

        console.log('[api] Retrieving dex.erc20 eth contract address from keystore...');

        // Ask name service for keystore scid
        const params = {
          scid: '0000000000000000000000000000000000000000000000000000000000000001',
          keysstring: ["keystore"],
        }
        // Call
        state.bridge
          .daemon("get-sc", params)
          .then(async (response: any) => {
            // Convert the value to a SCID
            let keystoreScid = response.data.result.valuesstring[0]
            keystoreScid = "80" + keystoreScid.substring(2, 64);

            console.log('[api] Asking keystore for network and addresses...');

            // Ask mainnet
            const response_network = await state.bridge.daemon("get-sc", {
              scid: keystoreScid,
              keysstring: ['k:mainnet']
            });
            // decode network
            let network: 'mainnet' | 'testnet' | null = null;
            if (response_network.data.result.valuesstring[0] == 1) {
              network = 'mainnet';
            } else if (response_network.data.result.valuesstring[0] == 0) {
              network = 'testnet';
            } else {
              throw response_network.data.result.valuesstring[0];
            }

            updateData({
              action: DataAction.UpdateNetwork,
              network
            })

            // Ask keystore for registry, bridge registry and ERC20 bridge addresses
            const base_keys = [
              "dex.bridge.erc20", // 0: encoded Ethereum bridge address
              "dex.bridge.registry", // 1: reverse bridge registry 
              "dex.swap.registry", // 2: dex registry adress
            ]

            const params = {
              scid: keystoreScid,
              keysstring: base_keys.map((k) => 'k:' + k)
            }

            // Call
            const response_keys = await state.bridge.daemon("get-sc", params);

            let keys: { [k: string]: string } = {}
            base_keys.forEach((k, index) => {
              keys[k] = response_keys.data.result.valuesstring[index];
            })

            // Decode hexdump
            keys['dex.bridge.erc20'] = decode(keys['dex.bridge.erc20'])
            console.log('[api] decoded keys', { network, keys });

            updateData({
              action: DataAction.UpdateKeys,
              keys
            })

            // Save address
            //setBridgeAddress(bridgeAddress)

          }).catch((error: any) => {
            console.error(error);
            toast.error(JSON.stringify(error));
          })
      }
    }, waitTimes.beforeFetching)

    return () => { clearTimeout(timeout) }

  }, [ready])


  // registry
  useEffect(() => {
    setTimeout(() => {
      if (ready && data.metadata.network && data.metadata.keys) {
        console.log("[api] fetching registry");
        const network = data.metadata.keys['network'];
        const params = {
          scid: data.metadata.keys['dex.swap.registry'],
          code: false,
          variables: true,
        };
        state.bridge
          .daemon("get-sc", params)
          .then((response: GetSCResponse) => {
            console.log({ dexRegistry: response.data.result });
            if (response.data.result.stringkeys === undefined) {
              toast.error(
                "Smart contract not found. Please make sure your wallet and daemon are both connected on " +
                network +
                ".",
                { autoClose: false }
              );
            } else {
              updateData({
                action: DataAction.UpdateRegistry,
                result: response.data.result,
              });
            }
          })
          .catch((err: any) => {
            console.error(err);
          });
      }
    }, waitTimes.beforeFetching);
  }, [ready, data.metadata.network, data.metadata.keys]);


  // bridge registry
  useEffect(() => {
    setTimeout(() => {
      if (ready && data.metadata.network && data.metadata.keys) {
        console.log("[api] fetching bridge registry");
        const network = data.metadata.keys['network'];
        const params = {
          scid: data.metadata.keys['dex.bridge.registry'],
          code: false,
          variables: true,
        };
        state.bridge
          .daemon("get-sc", params)
          .then((response: GetSCResponse) => {
            console.log({ bridgeRegistry: response.data.result });
            if (response.data.result.stringkeys === undefined) {
              toast.error(
                "Smart contract not found. Please make sure your wallet and daemon are both connected on " +
                network +
                ".",
                { autoClose: false }
              );
            } else {
              updateData({
                action: DataAction.UpdateBridgeRegistry,
                result: response.data.result,
              });


            }
          })
          .catch((err: any) => {
            console.error(err);
          });
      }
    }, waitTimes.beforeFetching);
  }, [ready, data.metadata.network, data.metadata.keys]);

  const readyForPairContracts = data.metadata.pairs.count > 0;

  // Fetch the contracts once available
  useEffect(() => {
    if (readyForPairContracts) {
      console.log("[api] fetching pair contracts...");

      Object.values(data.pairs).forEach((pair, pairIndex) => {
        setTimeout(() => {
          const params = {
            scid: pair.registry.contract,
            code: false,
            variables: true,
          };
          state.bridge
            .daemon("get-sc", params)
            .then((response: GetSCResponse) => {
              setPairContractsLoaded(pairIndex + 1);

              updateData({
                action: DataAction.UpdatePairContract,
                result: response.data.result,
                pair: `${pair.registry.name1}:${pair.registry.name2}`,
              });

              console.log(
                "[api] fetched contract for pair",
                pair.registry.name1,
                pair.registry.name2,
                response.data.result
              );
            })
            .catch((err: any) => {
              console.error(err);
            });
        }, waitTimes.beforeFetching + pairIndex * waitTimes.betweenFetches);
      });
    }
  }, [readyForPairContracts, state, userDataFetches]);

  // Fetch the asset balances once contracts finished loading
  useEffect(() => {
    if (
      pairContractsLoaded > 0 &&
      pairContractsLoaded == data.metadata.pairs.count
    ) {
      console.log("[api] fetching asset balances");

      // Get asset pair contracts
      const assets = data.metadata.tokens.array.map((name) => {
        const entry = Object.entries(data.pairs).find(
          ([pairName, pairData]: [string, PieSwapPairDataType]) => {
            return pairName.includes(name);
          }
        );
        if (name == entry?.[1].registry.name2) {
          return { name, asset: entry?.[1].registry.asset2 };
        } else {
          return { name, asset: entry?.[1].registry.asset1 };
        }
      });
      let error = false;
      // Fetch assets balances
      assets.forEach(({ name, asset }, index) => {
        
        setTimeout(() => {
          if (error) {return;}
          const params = {
            scid: asset,
          };

          state.bridge
            .wallet("get-balance", params)
            .then((response: GetBalanceResponse) => {
              if(response.data.error !== undefined) {
                error = true;
                toast.error('Error while getting balance: ' + response.data.error.message)
              } else {
                updateUserData({
                  action: UserDataAction.UpdateAssetBalance,
                  result: {
                    name: name,
                    content: response.data.result,
                  },
                  scData: data,
                });
              }
              
              
            })
            .catch((err: any) => {
              console.error(err);
            });
        }, waitTimes.beforeFetching + index * waitTimes.betweenFetches);
      });

      // Get pair contracts
      const pools = Object.entries(data.pairs).map(
        ([pairName, pairData]: [string, PieSwapPairDataType]) => {
          return { pairName, pool: pairData.registry.contract };
        }
      );

      // Fetch pair contract balances (user shares)
      pools.forEach(({ pairName, pool }, index) => {
        setTimeout(() => {
          const params = {
            scid: pool,
          };

          state.bridge
            .wallet("get-balance", params)
            .then((response: GetBalanceResponse) => {

              updateUserData({
                action: UserDataAction.UpdatePairBalance,
                result: {
                  name: pairName,
                  content: response.data.result,
                },
                scData: data,
              });
            })
            .catch((err: any) => {
              console.error(err);
            });
        }, assets.length * waitTimes.betweenFetches + waitTimes.beforeFetching * 2 + index * waitTimes.betweenFetches);
      });

    }
  }, [pairContractsLoaded]);


  useEffect(() => {
    const interval = setInterval(() => {
      console.log('[api] Fetching user data');

      fetchUserData()
    }, waitTimes.periodicFetches)
    return () => { clearInterval(interval) }
  }, [])

  return (
    <PieSwapDataContext.Provider value={{ data, updateData }}>
      <UserDataContext.Provider value={{ user: userData, updateUserData, fetchUserData }}>
        {props.children}
      </UserDataContext.Provider>
    </PieSwapDataContext.Provider>
  );
}
