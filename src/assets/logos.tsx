// Links to crypto currencies and tokens logos
export const tokenLogos: any = {
  DWETH: "https://cryptologos.cc/logos/ethereum-eth-logo.png?v=023",
  DZRX: "https://cryptologos.cc/logos/0x-zrx-logo.png?v=023",
  DUSDT: "https://cryptologos.cc/logos/tether-usdt-logo.png?v=023",
  DDAI: "https://cryptologos.cc/logos/multi-collateral-dai-dai-logo.png?v=023",
  DLINK: "https://cryptologos.cc/logos/chainlink-link-logo.png?v=023",
  DUSDC: "https://cryptologos.cc/logos/usd-coin-usdc-logo.png?v=023",
  DWBTC: "https://cryptologos.cc/logos/wrapped-bitcoin-wbtc-logo.png?v=023",
  DFRAX: "https://cryptologos.cc/logos/frax-frax-logo.png?v=023",
  DgOHM: "https://imgs.search.brave.com/mS-KF2LDb6WXaSTQH7-MbxmuDuEnkwGwNkpgAOEUUsg/rs:fit:40:40:1/g:ce/aHR0cHM6Ly9hc3Nl/dHMuY29pbmdlY2tv/LmNvbS9jb2lucy9p/bWFnZXMvMjExMjkv/bGFyZ2UvdG9rZW5f/d3NPSE1fbG9nby5w/bmc_MTYzODc2NDkw/MA",
  DERO: "https://imgs.search.brave.com/ZDVXj_uLx1b3331Xfw_wDUWNQYTnf7MCgePV2Ry1u00/rs:fit:40:40:1/g:ce/aHR0cHM6Ly9hc3Nl/dHMuY29pbmdlY2tv/LmNvbS9jb2lucy9p/bWFnZXMvMzUyMi9s/YXJnZS9kZXJvX2xv/Z29fcG5nLnBuZz8x/NTgyMTgwMzkw",
};
