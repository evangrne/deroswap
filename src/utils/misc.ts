import { useState } from "react";
import { PieSwapData } from "./PieswapDataContext";

export function forEachAttribute(
  o: any,
  callback: (key: string, element: any, index: number, array: string[]) => void,
  thisArg?: any
): void {
  Object.keys(o).forEach((key, index, array) => {
    const element = o[key];
    callback(key, element, index, array);
  });
}

export const useCounter = (initialValue?: number): [number, () => void, () => void] => {
  const [_f, _set_f] = useState(initialValue || 0);
  return [
    _f,
    () => { _set_f(_f + 1); },
    () => { _set_f(0); },
  ]
};

export function useModal() {
  const [_is, _set] = useState(false);
  const [isOpen, onOpen, onClose] = [
    _is,
    () => {
      _set(true);
    },
    () => {
      _set(false);
    },
  ];
  return { isOpen, onOpen, onClose };
}

export function findPair(from: string, to: string, data: PieSwapData) {
  let pair = `${from}:${to}`;
  let pairData = data.pairs[pair];
  let reverse = false;
  if (pairData === undefined) {
    reverse = true;
    pair = `${to}:${from}`;
    pairData = data.pairs[pair];
  }
  return { pairData, reverse };
}

export async function wait(ms: number, endSuccess: boolean) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      (endSuccess ? resolve : reject)();
    }, ms);
  });
}


export function toCamelCase(account: string) {
  return account
}

export function fixedFloor(n: number, digits: number) {
  const string = n.toString()
  if (string.includes('.')) {
    let [int, decimal] = string.split('.');

    if (decimal.length > digits) {
      return Number.parseFloat(int + '.' + decimal.substring(0, digits))
    } 
  } 
  return n;
  
}