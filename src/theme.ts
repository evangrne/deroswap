import Modal from "react-modal";
import type { Theme } from 'theme-ui'

export const colors = {
  violet_blue_crayola: "#776DD2ff",
  blue_pigment: "#3631AEff",
  black: "#010001ff",
  sky_blue_crayola: "#66DAF3ff",
  battleship_grey: "#8A8B8Bff",
  purple_pizzazz: "#F855EEff",
  blackAlpha: (variant: number) => {
    return `rgba(0,0,0, ${variant / 1000})`
  },
  whiteAlpha: (variant: number) => {
    return `rgba(255,255,255, ${variant / 1000})`
  }
};

export const skew = 25;

export function hexToRgba(hex: string) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16),
    a: parseInt(result[4], 16) / 256,
  } : {r: 0, g: 0, b: 0, a: 1};
}


export const defaultModalStyles: Modal.Styles = {
  overlay: {
    background: colors.blackAlpha(500),
    backdropFilter: "blur(4px)",
  },
  content: {
    padding: "2em",
    color: 'white',
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    background: colors.blue_pigment,
    border: "none",
    outline: "none",
    borderRadius: ".5em",
  },
};

export const theme: Theme = {
  config: {
    useColorSchemeMediaQuery: false,
  },
  colors: {
    text: colors.whiteAlpha(900),
    modes: {
      dark: {
        text: colors.whiteAlpha(900)
      }
    }
  }
}