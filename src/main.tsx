import React from 'react'
import ReactDOM from 'react-dom/client'
import Modal from 'react-modal'
import { HashRouter } from 'react-router-dom'
import App from './App'
import './index.css'

Modal.setAppElement('#root')

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <HashRouter basename={"/"}>
      <App />
    </HashRouter>
  </React.StrictMode>
)
