/** @jsxImportSource theme-ui */

import { useContext, useEffect, useReducer, useState } from "react";
import { ImWarning } from "react-icons/im";
import Modal from "react-modal";
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
import { toast } from "react-toastify";
import {
  scAdd,
  scGetAddGasEstimate,
  scGetRemoveGasEstimate,
  scRemove,
} from "../api";
import { AmountSelector } from "../components/content/AmountSelector";
import { Card } from "../components/content/Card";
import { SearchInput } from "../components/content/SearchInput";
import { TokenAvatar } from "../components/content/TokenAvatar";
import { PercentageInput } from "../components/content/PercentageInput";
import { colors, defaultModalStyles } from "../theme";
import { ConnectionStatus, SwapAction, SwapActionType, SwapStateType } from "../types";
import { DeroBridgeApiContext } from "../utils/DeroBridgeApiContext";
import { findPair, fixedFloor, useModal, wait } from "../utils/misc";
import {
  getAssetsAndDigits,
  PieSwapDataContext,
  PieSwapPairDataType,
  UserDataContext,
} from "../utils/PieswapDataContext";
import { getDigitsAndRatio } from "./Swap";
import { Decimal } from "decimal.js"


function multDivToInt(I: number, valueTo: number, valueFrom: number) {
  Decimal.set({precision: 80});
  const dValueTo = new Decimal(valueTo);
  const dI = new Decimal(I);
  const dValueFrom = new Decimal(valueFrom);
  let result = dI.mul(dValueTo); 
  result = result.div(dValueFrom)
  return result.floor().toNumber();
}

function mulPow10toInt(n: number, exponent: number) {
  const string = n.toString()
  if (string.includes('.')) {
    const index = string.indexOf('.');
    const full = string.slice(0, index) + string.slice(index + 1) + Array(exponent).fill(0).join('');
    const res = full.slice(0, index + exponent)
    console.warn('mulPow10toInt', {n, exponent, res:Number.parseInt(res)});
    return Number.parseInt(res)
  }
  else { 
    console.warn('mulPow10toInt', {n, exponent, res:Number.parseInt(string + Array(exponent).fill(0).join(''))});
    
    return Number.parseInt(string + Array(exponent).fill(0).join(''))
  }
}

function divPow10fromInt(n: number, exponent: number) {
  const string = n.toString()
  const full = Array(exponent).fill(0).join('') + string;
  const res = full.slice(0, full.length - exponent) + '.' + full.slice(full.length - exponent)
  console.warn('divPow10fromInt', {n, exponent, res, result:Number.parseFloat(res)});
  return Number.parseFloat(res)
}

function addRemoveReducer(
  swapState: SwapStateType,
  { action, token, amount, data }: SwapActionType
) {
  switch (action) {
    case SwapAction.ChangeAmountFrom:
      if (amount !== undefined && data !== undefined) {

        const { pairData, reverse } = findPair(swapState.tokenFrom, swapState.tokenTo, data)
        if (pairData !== undefined && pairData.pairContract !== undefined) {
          if (pairData.pairContract.hasLiquidity) {
            const { digitsFrom, digitsTo } = getDigitsAndRatio(pairData, reverse)
            
            const [valueFrom, valueTo] = !reverse ? [pairData.pairContract.value1, pairData.pairContract.value2] : [pairData.pairContract.value2, pairData.pairContract.value1];
            

            const amountTo = divPow10fromInt(
              multDivToInt(
                mulPow10toInt(amount, digitsFrom),
                valueTo, 
                valueFrom
              ), 
              digitsTo
            );
            
            const newState: SwapStateType = {
              ...swapState,
              amountFrom: fixedFloor(amount, digitsFrom),
              amountTo: amountTo,
            };
            return newState;
          } else {
          }
        }
        const newState: SwapStateType = { ...swapState, amountFrom: amount };
        return newState;
      }

    case SwapAction.ChangeAmountTo:
      if (amount !== undefined && data !== undefined) {
        const { pairData, reverse } = findPair(swapState.tokenFrom, swapState.tokenTo, data)

        if (pairData !== undefined && pairData.pairContract !== undefined) {
          if (pairData.pairContract.hasLiquidity) {
            const { digitsFrom, digitsTo } = getDigitsAndRatio(pairData, reverse)
            const [valueFrom, valueTo] = !reverse ? [pairData.pairContract.value1, pairData.pairContract.value2] : [pairData.pairContract.value2, pairData.pairContract.value1];
            
            const amountFrom = divPow10fromInt(
              multDivToInt(
                mulPow10toInt(amount, digitsTo),
                valueFrom,
                valueTo, 
                
              ), 
              digitsFrom
            );
            
            const newState: SwapStateType = {
              ...swapState,
              amountFrom: amountFrom,
              amountTo: Number(fixedFloor(amount, digitsTo)),
            };
            return newState;
          } else {
          }
        }
        const newState: SwapStateType = { ...swapState, amountTo: amount };
        return newState;
      }

    case SwapAction.ChangeTokenFrom:
      if (token !== undefined) {
        const newState: SwapStateType = { ...swapState, tokenFrom: token };
        return newState;
      }

    case SwapAction.ChangeTokenTo:
      if (token !== undefined) {
        const newState: SwapStateType = { ...swapState, tokenTo: token };
        return newState;
      }
    default:
      console.warn(action, "not implemented. or value is undefined");

      return swapState;
  }
}



/** Data table to display pairs */
function PoolTable({ searchFilter }: { searchFilter: string }) {
  // Context loading
  const { data } = useContext(PieSwapDataContext);
  const { user, fetchUserData } = useContext(UserDataContext);
  const { state } = useContext(DeroBridgeApiContext);

  // Table header
  const header = ["Pair", "Rate", "Liquidity", '', "Shares", "Fee"];

  // State for selected pair
  const [selectedPair, setSelectedPair] = useState<{
    names: string;
    pairData: PieSwapPairDataType;
  }>();

  // Nothing is selected at first
  const initialSwapState: SwapStateType = {
    tokenFrom: selectedPair?.pairData.registry.name1 || "",
    tokenTo: selectedPair?.pairData.registry.name2 || "",
    amountFrom: 0,
    amountTo: 0,
  };

  // Pseudo swapState for pair (amounts but will not be able to change or switch the tokens)
  const [swapState, updateSwapState] = useReducer(
    addRemoveReducer,
    initialSwapState
  );

  // Update the swapState following the selection of a pair
  useEffect(() => {
    updateSwapState({
      action: SwapAction.ChangeTokenFrom,
      token: selectedPair?.pairData.registry.name1 || "",
      data,
    });
    updateSwapState({
      action: SwapAction.ChangeTokenTo,
      token: selectedPair?.pairData.registry.name2 || "",
      data,
    });
    updateSwapState({ action: SwapAction.ChangeAmountFrom, amount: 0, data });
    updateSwapState({ action: SwapAction.ChangeAmountTo, amount: 0, data });
  }, [selectedPair]);

  // Modal state management
  const { isOpen, onOpen, onClose } = useModal();

  // Remove percentage of shares state
  const [removePercent, setRemovePercent] = useState<number | null>(null);

  const [selectedPairStatus, setSelectedPairStatus] = useState<
    "no contract" | "no liquidity" | "ok" | "not loaded"
  >("not loaded");

  function getPairStatus(pairData: PieSwapPairDataType | undefined) {
    return pairData === undefined
      ? "not loaded"
      : pairData.pairContract === undefined
        ? "no contract"
        : pairData.pairContract?.hasLiquidity || undefined
          ? "ok"
          : "no liquidity";
  }

  useEffect(() => {
    setSelectedPairStatus(getPairStatus(selectedPair?.pairData || undefined));
  }, [selectedPair, data]);

  const [isProcessing, setProcessing] = useState<boolean | "done">(false);

  function startProcessing(tabName: ModalTabName) {
    const { pairData, reverse } = findPair(
      swapState.tokenFrom,
      swapState.tokenTo,
      data
    );
    if (
      pairData !== undefined &&
      pairData.pairContract !== undefined &&
      user != undefined
    ) {
      const { assetFrom, assetTo, digitsFrom, digitsTo } = getAssetsAndDigits(
        pairData,
        reverse
      );

      //wait(1500, Math.random() > 0.3)
      if (tabName == ModalTabName.Add) {
        scAdd(
          assetFrom,
          assetTo,
          mulPow10toInt(swapState.amountFrom, digitsFrom),
          mulPow10toInt(swapState.amountTo, digitsTo),
          state.bridge,
          pairData.registry.contract
        )
          .then((res) => {
            if (res.data.error !== undefined) {
              console.error(res.data.error.message);
              toast.error(`something went wrong while adding liquidity...`);
              setProcessing(false);
            } else {
              console.log(res);
              toast.success(`successfully added liquidity !`);
              setProcessing("done");
            }
          })
          .catch((err) => {
            toast.error(`something went wrong while adding liquidity...`);
            setProcessing(false);
          });
      } else {
        if (removePercent != null) {
          scRemove(pairData, user, removePercent, state.bridge)
            .then((res) => {
              if (res.data.error !== undefined) {
                console.error(res.data.error.message);
                toast.error(`something went wrong while removing liquidity...`);
                setProcessing(false);
              } else {
                console.log(res);
                toast.success(`successfully removed liquidity !`);
                setProcessing("done");
              }
              setProcessing("done");
            })
            .catch((err) => {
              toast.error(`something went wrong while removing liquidity...`);
              setProcessing(false);
            });
        }
      }
    }
  }

  enum ModalTabName {
    Add = "Add Liquidity",
    Remove = "Remove Liquidity",
  }

  const userHasPairLiquidity = user.pairs[`${selectedPair?.pairData.registry.name1}:${selectedPair?.pairData.registry.name2}`]?.balance == 0;
  const modalTabs: ModalTabName[] =
    selectedPairStatus == "no contract"
      ? []
      : userHasPairLiquidity || false
        ? [ModalTabName.Add]
        : selectedPairStatus == "no liquidity"
          ? [ModalTabName.Add]
          : selectedPairStatus == "not loaded"
            ? []
            : [ModalTabName.Add, ModalTabName.Remove];

  const [modalTabIndex, setModalTabIndex] = useState(0);

  const unavailableAmount = (() => {
    const userTokenDataFrom = user.balances.find((token) => token.name == swapState.tokenFrom)
    const userTokenDataTo = user.balances.find((token) => token.name == swapState.tokenTo)
    if (userTokenDataFrom && userTokenDataTo) {
      const userBalanceFrom = userTokenDataFrom.balance / Math.pow(10, userTokenDataFrom.digits);
      const userBalanceTo = userTokenDataTo.balance / Math.pow(10, userTokenDataTo.digits);
      return userBalanceFrom < swapState.amountFrom || userBalanceTo < swapState.amountTo
    } else { return true; }
  })();


  // Buttons at the bottom of the modal
  const tabPanelButtons = (tabName: ModalTabName) => {
    const actionButtonLabel = tabName == ModalTabName.Add ? "Add" : "Remove";
    const processingButtonLabel =
      tabName == ModalTabName.Add ? "adding..." : "removing...";

    const selectorsInvalid =
      tabName == ModalTabName.Add
        ? swapState.amountFrom == 0 && swapState.amountTo == 0 || unavailableAmount
        : removePercent == null || removePercent == 0;

    return (
      <div sx={{ justifyContent: "flex-end" }}>
        {isProcessing != false ? (
          <></>
        ) : (
          <button sx={{ background: "transparent", color: colors.whiteAlpha(900) }} onClick={onClose}>
            Cancel
          </button>
        )}
        <button
          sx={{
            background:
              isProcessing == false
                ? !selectorsInvalid
                  ? colors.sky_blue_crayola
                  : colors.whiteAlpha(300)
                : isProcessing == "done"
                  ? colors.violet_blue_crayola
                  : colors.whiteAlpha(300),
            color: colors.blackAlpha(900),
          }}
          onClick={() => {
            switch (isProcessing) {
              case "done":
                setProcessing(false);
                onClose();
                setTimeout(() => {
                  fetchUserData()
                }, 100);
                break;
              case false:
                setProcessing(true);
                startProcessing(tabName);
                break;
              default:
                break;
            }
          }}
          disabled={isProcessing == true || selectorsInvalid}
        >
          {isProcessing == true
            ? processingButtonLabel
            : isProcessing == "done"
              ? "Ok"
              : actionButtonLabel}
        </button>
      </div>
    );
  };

  // Gas estimates
  const [addGasEstimates, setAddGasEstimates] = useState<
    number | null | "error" | "invalid"
  >(null);

  useEffect(() => {
    const { pairData, reverse } = findPair(
      swapState.tokenFrom,
      swapState.tokenTo,
      data
    );
    if (pairData !== undefined && pairData.pairContract !== undefined) {
      const { assetFrom, assetTo, digitsFrom, digitsTo } = getAssetsAndDigits(
        pairData,
        reverse
      );

      const timeout = setTimeout(() => {
        setAddGasEstimates(null);
        const values = {
          valueFrom: mulPow10toInt(swapState.amountFrom, digitsFrom),
          valueTo: mulPow10toInt(swapState.amountTo, digitsTo),
        }
        console.log({pairData, assetFrom, assetTo, digitsFrom, digitsTo, values, state});
        scGetAddGasEstimate(
          assetFrom,
          assetTo,
          values,
          state.bridge,
          pairData.registry.contract
        )
          .then((res) => {
            setAddGasEstimates(res);
          })
          .catch((err) => {
            setAddGasEstimates(err);
          });
      }, 200);
      return () => {
        clearTimeout(timeout);
      };
    }
  }, [swapState, data]);

  const [removeGasEstimates, setRemoveGasEstimates] = useState<
    number | null | "error" | "invalid"
  >(null);

  const [{ userShares, sharesOutstanding }, setShares] = useState({
    userShares: 0,
    sharesOutstanding: 0,
  });

  // Gas estimates for removing pair
  useEffect(() => {
    const { pairData } = findPair(swapState.tokenFrom, swapState.tokenTo, data);
    if (pairData !== undefined && pairData.pairContract !== undefined) {
      if (removePercent != null) {
        // Get gas estimates
        const timeout = setTimeout(() => {
          if (pairData !== undefined && pairData.pairContract !== undefined && user.pairs) {
            setRemoveGasEstimates(null);
            console.log({pairData, removePercent, state});
            
            scGetRemoveGasEstimate(pairData, user, removePercent, state.bridge)
              .then((res) => {
                setRemoveGasEstimates(res);
              })
              .catch((err) => {
                setRemoveGasEstimates(err);
              });
          }
        }, 200);
        return () => {
          clearTimeout(timeout);
        };
      }
    }
  }, [removePercent, data, modalTabIndex, isOpen]);

  const [liquidity, setLiquidity] = useState<{
    amountFrom: number;
    amountTo: number;
    totalFrom: string;
    totalTo: string;
    digitsFrom: number;
    digitsTo: number;
  }>({
    amountFrom: Number.NaN,
    amountTo: Number.NaN,
    totalFrom: "-",
    totalTo: "-",
    digitsFrom: Number.NaN,
    digitsTo: Number.NaN,
  });

  useEffect(() => {
    const { pairData, reverse } = findPair(
      swapState.tokenFrom,
      swapState.tokenTo,
      data
    );
    const pairName = `${swapState.tokenFrom}:${swapState.tokenTo}`;
    if (pairData !== undefined && pairData.pairContract !== undefined) {
      console.log("[api] computing amounts from percentage...");

      let bal1, bal2;
      if (pairData.pairContract.shares != 0) {
        bal1 = Number(
          (BigInt(pairData.pairContract.value1) *
            BigInt(pairData.pairContract.balance)) /
          BigInt(pairData.pairContract.shares)
        );
        bal2 = Number(
          (BigInt(pairData.pairContract.value2) *
            BigInt(pairData.pairContract.balance)) /
          BigInt(pairData.pairContract.shares)
        );
      } else {
        (bal1 = 0), (bal2 = 0);
      }

      const { digitsFrom, digitsTo } = getAssetsAndDigits(pairData, reverse);

      const amountFrom = bal1 / Math.pow(10, digitsFrom);
      const amountTo = bal2 / Math.pow(10, digitsFrom);

      const totalFrom = (
        pairData.pairContract.value1 / Math.pow(10, digitsFrom)
      ).toString();

      const totalTo = (
        pairData.pairContract.value2 / Math.pow(10, digitsTo)
      ).toString();

      setLiquidity({
        amountFrom,
        amountTo,
        totalFrom,
        totalTo,
        digitsFrom,
        digitsTo,
      });

      // Get shares
      setShares({
        userShares: user.pairs[pairName].balance,
        sharesOutstanding: pairData.pairContract.shares,
      });
    }
  }, [data, modalTabIndex, isOpen]);

  // ------------------ MODAL
  const modal = (
    <Modal
      isOpen={isOpen}
      onRequestClose={() => {
        if (isProcessing == false) {
          onClose();
          setModalTabIndex(0);
        }
      }}
      onAfterOpen={() => { }}
      style={{
        content: { ...defaultModalStyles.content, width: "80%" },
        overlay: defaultModalStyles.overlay,
      }}
    >
      <div
        sx={{
          flexDirection: "column",
          gap: "1em",
          fontFamily: "monospace",
          width: "100%",
        }}
      >
        <div
          sx={{
            gap: "1em",
            alignItems: "center",
            flexDirection: "column",
            fontSize: ".9em",
            width: "100%",
          }}
        >
          <div
            sx={{
              fontWeight: "bold",
              fontSize: "1.1em",
              justifyContent: "flex-start",
              width: "100%",
            }}
          >
            {selectedPair?.names}
          </div>

          <Tabs
            style={{ flexDirection: "column", width: "100%" }}
            selectedIndex={modalTabIndex}
            onSelect={(index) => {
              if (isProcessing == false) {
                updateSwapState({
                  action: SwapAction.ChangeAmountFrom,
                  amount: 0,
                  data,
                });
                updateSwapState({
                  action: SwapAction.ChangeAmountTo,
                  amount: 0,
                  data,
                });
                setRemovePercent(null);
                setModalTabIndex(index);
              }
            }}
          >
            <TabList
              style={{
                display: "flex",
                userSelect: "none",
                padding: "0",
                width: "100%",
                alignContent: "flex-start",
                borderBottom: "1px solid " + colors.whiteAlpha(200),
              }}
            >
              {modalTabs.map((name, index) => {
                return (
                  <Tab
                    key={name}
                    style={{
                      display: "inline-block",
                      borderRadius: ".5em .5em 0 0 ",
                      padding: "1em",
                      background:
                        modalTabIndex == index
                          ? colors.violet_blue_crayola
                          : colors.whiteAlpha(100),
                    }}
                  >
                    {name}
                  </Tab>
                );
              })}
            </TabList>
            {modalTabs.includes(ModalTabName.Add) ? (
              <TabPanel style={{ flexDirection: "column", gap: "1em" }}>
                <h2>Add liquidity</h2>
                {selectedPairStatus == "no liquidity" ? (
                  <div
                    sx={{
                      alignItems: "center",
                      gap: "1em",
                      flexWrap: "wrap",
                    }}
                  >
                    <ImWarning /> You are adding the first liquidity for the
                    pair. Choose the rate wisely.
                  </div>
                ) : (
                  ""
                )}
                <div
                  sx={{
                    justifyContent: "space-around",
                    gap: ".5em",
                    alignItems: "center",
                    flexWrap: "wrap",
                  }}
                >
                  <AmountSelector
                    state={swapState}
                    update={updateSwapState}
                    _key={"from"}
                    disableTokenSelection
                  />
                  {/*<ImArrowLeft />*/}
                  {/*<ImArrowRight />*/}
                  <AmountSelector
                    state={swapState}
                    update={updateSwapState}
                    _key={"to"}
                    disableTokenSelection
                  />
                </div>
                <div>
                  Estimated gas fees :{" "}
                  {(() => {
                    switch (addGasEstimates) {
                      case null:
                        return (
                          <i sx={{ color: colors.whiteAlpha(500) }}>
                            ...computing...
                          </i>
                        );
                      case "error":
                        return "?";
                      case "invalid":
                        return "-";
                      default:
                        return <>{addGasEstimates / 100000} DERO</>;
                    }
                  })()}
                </div>

                {tabPanelButtons(ModalTabName.Add)}
              </TabPanel>
            ) : (
              <></>
            )}
            {modalTabs.includes(ModalTabName.Remove) ? (
              <TabPanel style={{ flexDirection: "column", gap: "1em" }}>
                <div style={{ flexDirection: "column", gap: "1em" }}>
                  <h2>Remove liquidity</h2>
                  <div
                    sx={{
                      justifyContent: "center",
                      gap: ".5em",
                      alignItems: "center",
                      flexWrap: "wrap",
                    }}
                  >
                    <PercentageInput
                      value={removePercent}
                      onChange={(event) => {
                        if (event.target.value != "") {
                          let value = Number(event.target.value);
                          if (Number.isNaN(value) || value < 0) {
                            value = 0;
                          } else if (value > 100) {
                            value = 100;
                          }
                          setRemovePercent(value);
                        } else {
                          setRemovePercent(null);
                        }
                      }}
                    />
                    <span style={{fontSize:'16pt'}}>%</span>
                  </div>
                </div>
                <div
                  sx={{
                    justifyContent: "space-around",
                    gap: "2em",
                    flexWrap: "wrap",
                  }}
                >
                  <table sx={{ width: "40%" }}>
                    <thead>
                      <tr>
                        <th colSpan={2}>TO BE REMOVED</th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <th
                          sx={{
                            paddingX: "2em",
                            borderBottom: "1px solid " + colors.whiteAlpha(200),
                          }}
                        >
                          <div sx={{ alignItems: "center", gap: ".3em" }}>
                            <TokenAvatar
                              name={
                                selectedPair
                                  ? selectedPair.pairData.registry.name1
                                  : ""
                              }
                              imageSize={24}
                            />
                            {selectedPair?.pairData.registry.name1}
                          </div>
                        </th>
                        <td
                          sx={{
                            paddingX: "2em",
                            borderBottom: "1px solid " + colors.whiteAlpha(200),
                          }}
                        >
                          {removePercent == null
                            ? "-"
                            : Number(
                              (
                                Number(liquidity.totalFrom) *
                                (userShares / sharesOutstanding) *
                                (removePercent / 100)
                              ).toFixed(liquidity.digitsFrom)
                            )}
                        </td>
                      </tr>
                      <tr>
                        <th
                          sx={{
                            borderBottom: "1px solid " + colors.whiteAlpha(200),
                            paddingX: "2em",
                          }}
                        >
                          <div sx={{ alignItems: "center", gap: ".3em" }}>
                            <TokenAvatar
                              name={
                                selectedPair
                                  ? selectedPair.pairData.registry.name2
                                  : ""
                              }
                              imageSize={24}
                            />
                            {selectedPair?.pairData.registry.name2}
                          </div>
                        </th>
                        <td
                          sx={{
                            paddingX: "2em",
                            borderBottom: "1px solid " + colors.whiteAlpha(200),
                          }}
                        >
                          {removePercent == null
                            ? "-"
                            : Number(
                              Number(liquidity.totalTo) *
                              (userShares / sharesOutstanding) *
                              (removePercent / 100)
                            ).toFixed(liquidity.digitsTo)}
                        </td>
                      </tr>
                      <tr>
                        <th
                          sx={{
                            borderBottom: "1px solid " + colors.whiteAlpha(200),
                            paddingX: "2em",
                          }}
                        >
                          shares
                        </th>
                        <td
                          sx={{
                            paddingX: "2em",
                            borderBottom: "1px solid " + colors.whiteAlpha(200),
                          }}
                        >
                          {removePercent == null
                            ? "-"
                            : (removePercent / 100) * userShares}
                        </td>
                      </tr>
                      <tr>
                        <th
                          sx={{
                            paddingX: "2em",
                          }}
                        >
                          % of total shares
                        </th>
                        <td
                          sx={{
                            paddingX: "2em",
                          }}
                        >
                          {removePercent == null
                            ? "-"
                            : (
                              (removePercent * userShares) /
                              sharesOutstanding
                            ).toFixed(2)}{" "}
                          %
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  {/*<table sx={{ width: "40%" }}>
                    <thead>
                      <tr>
                        <th colSpan={2}>TOTAL IN THE POOL</th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <th
                          sx={{
                            paddingX: "2em",
                            borderBottom: "1px solid " + colors.whiteAlpha(200),
                          }}
                        >
                          <div sx={{ alignItems: "center", gap: ".3em" }}>
                            <TokenAvatar
                              name={
                                selectedPair
                                  ? selectedPair.pairData.registry.name1
                                  : ""
                              }
                              imageSize={24}
                            />
                            {selectedPair?.pairData.registry.name1}
                          </div>
                        </th>
                        <td
                          sx={{
                            paddingX: "2em",
                            borderBottom: "1px solid " + colors.whiteAlpha(200),
                          }}
                        >
                          {liquidity.totalFrom.toLowerCase()}
                        </td>
                      </tr>
                      <tr>
                        <th
                          sx={{
                            borderBottom: "1px solid " + colors.whiteAlpha(200),
                            paddingX: "2em",
                          }}
                        >
                          <div sx={{ alignItems: "center", gap: ".3em" }}>
                            <TokenAvatar
                              name={
                                selectedPair
                                  ? selectedPair.pairData.registry.name2
                                  : ""
                              }
                              imageSize={24}
                            />
                            {selectedPair?.pairData.registry.name2}
                          </div>
                        </th>
                        <td
                          sx={{
                            paddingX: "2em",
                            borderBottom: "1px solid " + colors.whiteAlpha(200),
                          }}
                        >
                          {liquidity.totalTo.toLowerCase()}
                        </td>
                      </tr>
                      <tr>
                        <th
                          sx={{
                            borderBottom: "1px solid " + colors.whiteAlpha(200),
                            paddingX: "2em",
                          }}
                        >
                          my shares
                        </th>
                        <td
                          sx={{
                            paddingX: "2em",
                            borderBottom: "1px solid " + colors.whiteAlpha(200),
                          }}
                        >
                          {userShares}
                        </td>
                      </tr>
                      <tr>
                        <th
                          sx={{
                            paddingX: "2em",
                          }}
                        >
                          total shares
                        </th>
                        <td
                          sx={{
                            paddingX: "2em",
                          }}
                        >
                          {sharesOutstanding}
                        </td>
                      </tr>
                    </tbody>
                  </table>*/}
                </div>
                <div
                  sx={{
                    marginTop: "1em",
                    paddingTop: "1em",
                    borderTop: "1px solid " + colors.whiteAlpha(50),
                  }}
                >
                  Estimated gas fees :{" "}
                  {removeGasEstimates == "error"
                    ? "?"
                    : removeGasEstimates == "invalid"
                      ? "-"
                      : removeGasEstimates == null
                        ? "-"
                        : removeGasEstimates / 100000}{" "}
                  DERO
                </div>
                {tabPanelButtons(ModalTabName.Remove)}
              </TabPanel>
            ) : (
              <></>
            )}
          </Tabs>
          {selectedPairStatus == "no contract" ? (
            <div>
              No contract could be retrieved. You might need to wait or refresh
              the page.
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
    </Modal>
  );

  return (
    <>
      <table>
        <thead
          sx={{
            fontFamily: "Helvetica",
            fontSize: ".8em",
            color: colors.whiteAlpha(300),
          }}
        >
          <tr sx={{ textAlign: "left", fontWeight: "bold" }}>
            {header.map((label) => {
              return <td key={label}>{label.toUpperCase()}</td>;
            })}
          </tr>
        </thead>
        <tbody>
          {Object.entries(data.pairs)
            .filter(([names, _]) => {
              return searchFilter.length == 0
                ? true
                : names.toLowerCase().includes(searchFilter.toLowerCase());
            })
            .map(([names, pairData], index) => {
              const odd = (index + 1) % 2;
              const ratio =
                pairData.pairContract?.ratio1to2.toFixed(
                  pairData.registry.digits2
                ) || "?";
              const [name1, name2] = [
                pairData.registry.name1,
                pairData.registry.name2,
              ];
              const pairStatus = getPairStatus(pairData);
              const rate =
                pairStatus == "not loaded" ? (
                  <i sx={{ color: colors.whiteAlpha(400) }}>...</i>
                ) : pairStatus == "no contract" ? (
                  <i sx={{ color: colors.whiteAlpha(400) }}>
                    No pair contract was loaded
                  </i>
                ) : pairStatus == "no liquidity" ? (
                  <i sx={{ color: colors.whiteAlpha(400) }}>No liquidity</i>
                ) : (
                  `1 ${name1} ≈ ${ratio} ${name2}`
                );

              const [shares, fee] = [
                pairData.pairContract?.shares,
                (pairData.pairContract?.poolFee || null) == null
                  ? ""
                  : `${(pairData.pairContract?.poolFee || 0) / 100} %`,
              ];

              const pair = user.pairs[name1 + ':' + name2];
              const myshares = pair?.balance;


              const { digitsFrom, digitsTo } = getAssetsAndDigits(pairData, false);

              // TODO continue here
              const totalFrom = pairData.pairContract?.value1 ? (
                pairData.pairContract?.value1 / Math.pow(10, digitsFrom)
              ).toString() : '-';

              const totalTo = pairData.pairContract?.value2 ? (
                pairData.pairContract?.value2 / Math.pow(10, digitsTo)
              ).toString() : '-';

              const tokenImageSize = 48;
              const tokenImageSizePx = `${tokenImageSize}px`;
              return (
                <tr
                  key={names}
                  sx={{
                    background: odd ? "transparent" : colors.whiteAlpha(30),
                    "&:hover": { background: colors.whiteAlpha(60) },
                    textAlign: "left",
                  }}
                  onClick={() => {
                    setSelectedPair({ names, pairData });
                    onOpen();
                  }}
                >
                  <td
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      gap: "1em",
                      colorScheme: "dark",
                      height: tokenImageSizePx,
                    }}
                  >
                    <div sx={{}}>
                      <div
                        style={{
                          height: tokenImageSizePx,
                          width: tokenImageSizePx,
                          borderRadius: tokenImageSizePx,
                          backgroundColor: colors.sky_blue_crayola,
                          alignItems: "center",
                        }}
                      >
                        <TokenAvatar name={name1} imageSize={tokenImageSize} />
                      </div>
                      <div
                        style={{
                          position: "relative",
                          left: "-12px",
                          height: tokenImageSizePx,
                          width: tokenImageSizePx,
                          borderRadius: tokenImageSizePx,
                          backgroundColor: colors.violet_blue_crayola,
                          alignItems: "center",
                        }}
                      >
                        <TokenAvatar name={name2} imageSize={tokenImageSize} />
                      </div>
                    </div>
                    {names}
                  </td>
                  <td>{rate}</td>
                  <td>{
                    Number(totalFrom) > 0 ?

                      <div style={{ gap: '.5em', alignItems: 'center', justifyContent: 'flex-start' }}>
                        <TokenAvatar name={name1} imageSize={24} />
                        {totalFrom}
                      </div>

                      : <></>}</td>
                  <td>{
                    Number(totalTo) > 0 ?

                      <div style={{ gap: '.5em', alignItems: 'center', justifyContent: 'flex-start' }}>
                        <TokenAvatar name={name2} imageSize={24} />
                        {totalTo}
                      </div>

                      : <></>}</td>

                  <td>
                    {Number(totalFrom) > 0 ?
                      <a title={`${myshares} / ${shares}`} style={{ color: 'inherit', textDecoration: 'none' }}><div style={{ justifyContent: 'flex-end', paddingRight: '.5em' }}>
                        {
                          myshares == 0 ?
                            <span style={{ color: colors.whiteAlpha(200) }}>0.0 %</span>
                            : shares === undefined || shares == 0 ? '0.0' : myshares / shares > 0.001 ? (Math.round(1000 * myshares / shares) / 10).toFixed(1) + ' %' : myshares == undefined ? '' :
                              <span style={{ color: colors.whiteAlpha(600) }}>{'< 0.1 %'}</span>
                        }
                      </div>
                      </a> : <></>}
                  </td>
                  <td>
                    {Number(totalFrom) > 0 ?
                      <div style={{ justifyContent: 'flex-end', paddingRight: '.5em' }}>
                        {fee}
                      </div>
                      : <></>}
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
      {modal}
    </>
  );
}

export default function Pool() {
  const { state, update } = useContext(DeroBridgeApiContext);

  const ready =
    state.daemon == ConnectionStatus.Ready &&
    state.wallet == ConnectionStatus.Ready;

  const [pairSearchText, setPairSearchText] = useState("");

  if (ready) {
    return (
      <Card title="pool" width="640px">
        <div sx={{ justifyContent: "flex-start", width: "100%" }}>
          <SearchInput
            label="Filter pairs"
            onChange={(event) => {
              setPairSearchText(event.target.value);
            }}
          />
        </div>
        <PoolTable searchFilter={pairSearchText} />
      </Card>
    );
  }
  return <>Wallet or daemon are not yet setup... </>;
}