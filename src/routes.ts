import Swap from "./routes/Swap";
import Pool from "./routes/Pool";
import Bridge from "./routes/Bridge";
import Assets from "./routes/Assets";
import Charts from "./routes/Charts";


export const routes = [
  { to: "/assets", label: "Assets", element: Assets, disabled: false },
  { to: "/swap", label: "Swap", element: Swap, disabled: false },
  { to: "/pool", label: "Pool", element: Pool, disabled: false },
  { to: "/bridge", label: "bridge", element: Bridge, disabled: false },
  //{ to: "/vote", label: "Vote", element: <Vote />, disabled: true },
  { to: "/charts", label: "Charts", element: Charts, disabled: false },
];
