import { useEffect, useState } from "react";
import { Button, Image, Link, Text } from "theme-ui";
import { logo } from "../../assets/base64logo";
import { routes } from "../../routes";
import { colors, skew } from "../../theme";
import { DeroConnector } from "./DeroConnector";

const nav_height = "3em";
const side_size = "20em";

// Navigation button component
function NavButton(props: {
  name: string;
  active?: boolean;
  disabled?: boolean;
  selectRoute: any;
}) {
  const onButtonClick = () => {
    props.selectRoute(`#/${props.name}`);
  };

  return (
    <Link href={"#/" + props.name} style={{ flexGrow: "1" }}>
      <Button
        style={{
          height: nav_height,
          width: "100%",
          background: props.active ? colors.whiteAlpha(700) : "transparent",
          borderRadius: "4px",
          margin: "0 1px",
        }}
        onClick={onButtonClick}
      >
        <div
          style={{
            transform: `skew(${skew}deg)`,
            color: colors.blackAlpha(
              props.disabled ? 400 : props.active ? 1000 : 800
            ),
            fontWeight: "bold",
            userSelect: "none",
          }}
        >
          <Text>{props.name}</Text>
        </div>
      </Button>
    </Link>
  );
}

// Navigation component
function Nav() {
  const [selectedRoute, setSelectedRoute] = useState("swap");

  useEffect(() => {
    setSelectedRoute(window.location.hash ? window.location.hash : "");
  }, []);

  return (
    <nav
      className="drop-shadow"
      style={{
        flexWrap: "wrap",
        background: colors.sky_blue_crayola,
        transform: `skew(-${skew}deg)`,
        borderRadius: "4px",
        padding: "2px",
        marginLeft: "2em",
        marginRight: "2em",
      }}
    >
      {routes.map((r) => {
        const name = r.label.toLowerCase();
        const active = selectedRoute == `#${r.to}`;

        return (
          <NavButton
            key={name}
            active={active}
            disabled={r.disabled}
            name={name}
            selectRoute={setSelectedRoute}
          />
        );
      })}
    </nav>
  );
}

// Header component
export function Header() {
  return (
    <header
      style={{
        padding: "2em 0",
        flexWrap: "wrap",
        justifyContent: "space-around",
        alignItems: "center",
        flexDirection: "row",
        gap: "2em",
      }}
    >
      <Link href={"#/"}>
        <div
          className="drop-shadow"
          style={{ height: nav_height, width: side_size }}
        >
          <Image src={logo} />
        </div>
      </Link>

      <Nav />

      <DeroConnector width={side_size} />
    </header>
  );
}
