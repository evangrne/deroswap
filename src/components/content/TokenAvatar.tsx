import { tokenLogos } from "../../assets/logos";

type TokenAvatarProps = {
  name: string;
  imageSize: number;
};

export function TokenAvatar({ name, imageSize }: TokenAvatarProps) {
  const tokenImageSize = `${imageSize}px`;
  const actualImageSize = `${(3 * imageSize) / 4}px`;
  return (
    <img
      className="drop-shadow"
      src={tokenLogos[name]}
      width={actualImageSize}
      height={actualImageSize}
      onError={(event) => {
        event.currentTarget.style.background = "white";
      }}
      style={{ borderRadius: tokenImageSize }}
    />
  );
}
